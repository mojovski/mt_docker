# ZMQ-MT Api

The API is implemented in MT as an EA. The code can be found in the file `zmq_trader.mq5`.
The ZMQ-Trader-EA listens for commands over ZMQ, which are formatted as json strings.
The following descriptions define the commands and the json protocols.

## General example

In order to send a command to MT, a `dict` is created and submitted via the method `sendToMt(msg)` in the MetatraderConnector class.
The result should be also a dict, but you should check for consistency.
Here is an example on subscribing MT to listening and streaming ticker data for `EURUSD`.

```py
msg = {'cmd': 'setSymbols', 'symbols': ['EURUSD']}
res = self.sendToMt(msg)
if res == None:
	raise Exception("The response on setSymbols is not valid: "+str(res))
```


## setSymbols

Defines a list of symbols ot be tracked by MT. Ticker of each tracked symbol is streamed over zmq.


* cmd: `setSymbols`
* symbols: uppercased symbols supported by your broker (e.g. `EURUSD`).

**Example**:

```py
msg = {'cmd': 'setSymbols', 'symbols': self.symbols}
res = self.sendToMt(msg)
```



## addSymbol

Append one symbol to the tracking list in MT.

* cmd: `addSymbol`
* symbol: uppercased symbol supported by your broker (e.g. `EURUSD`).

**Example**:

```py
msg = {'cmd': 'addSymbol', 'symbol': symbol.upper()}
res = self.sendToMt(msg)
```

## getAccountInfo

Returns the balance, open pnl and margin data from MT.

```py
msg = {'cmd': 'getAccountInfo'}
```


## placeOrder

Placing a new order
* cmd: `placeOrder`
* order:
	* symbol: asset symbol
	* type: Order type id according to [the MQL definition](https://www.mql5.com/de/docs/constants/tradingconstants/orderproperties#enum_order_type)
	* lots: Position size in lots
	* price: price if this is a limit or stop order
	* SL: stop loss price
	* TP: take profit price
	* comment: Commment to be stored in MT
	* magic: magic number to be stored in MT
	* max_slippage: max deviation from the price (for limit or stop order).

```py
order_msg = {'symbol': order.symbol.upper(), 'type': 0, 'lots': 0.01,
            'price': float(order.price), 'SL': sl, 'TP': tp, 'comment': order.descr, 'magic': 0,
            'max_slippage': 2}
msg = {'cmd': 'placeOrder', 'order': order_msg}
```


## getOpenOrders

Returns open positions including the pnl, sl, tp and the comment of each position.

* cmd: `getOpenPositions`

**Example:**:

```py
msg = {'cmd': 'getOpenOrders'}
```


## closePosition

Closes an open position.

* cmd: `closePosition`
* position:
	* order_id: the ID of the position (shown on the left in the trade tab). Also known as the ticket in the MQL/MT wording.
	* slippage: max deviation from the target exit price - but not sure how it works on the broker side. Int value.
	* qty: In case you would like to close only a part of the position, set `qty` to a lot value smaller than the full lot size of the position. If the position has a lot value of 1 lot, and you are 100€ in profit, and close here `qty:0.1`, then you will book 0.1(10€) and keep 0.9(90€) open.

**Example:**

```py
msg = {'cmd': 'closePosition', 'position': {'order_id': int(
order.exchange_uid), 'slippage': max_slippage, 'qty': float(np.abs(order.qty))}}
```

## getPastTicks

Returns past tick data. The amount of data however is limited by the broker.

* cmd: ` getPastTicks`
* symbol: Upper cased asset symbol
* num: number of ticks to receive
* start_time: the time from when on the ticks should be acquired (unix time, int).

**Example:**

```py
if start_time != None:
    unix_time = int(start_time.timestamp()*1e3)
else:
    unix_time = 0
msg = {'cmd': 'getPastTicks', 'symbol': symbol,
            'num': num, 'start_time': unix_time}
```

## getPastCandles

Returns the historical candle data.

* cmd: `getPastCandles`
* symbol: upper cased asset symbol
* num: Amount of candles to receive
* timeframe: one of these: `1m`, `5m`, `15m`, `30m`, `1H`, `4H`, `1d` as string.
* start and end time: use this format : yyyy.mm.dd [hh:mi] 
	according to [the MQL doc](https://www.mql5.com/en/docs/convert/stringtotime)

**Example:**

```py
msg = {'cmd': 'getPastCandles', 'symbol': symbol.upper(), 'num': num,
            'timeframe': timeframe}

if start_time != None and end_time != None:
    msg = {'cmd': 'getPastCandlesRange', 'symbol': symbol.upper(
    ), 'start_time': start_time, 'end_time': end_time, 'timeframe': timeframe}
```


## Receiving Tick Streams

In order to get the real time ticker data, you will need to obtain them via zmq subscription.
In short, two steps are required:
1. Call `setSymbols` in order to specify the symbols you would like to get streamed.
2. Create a subscription socket like this and handle new messages:

```py
self.sub_socket = self.context.socket(zmq.SUB)
# in milliseconds. set it to a high value, since it depends on how often new prices arrive
self.sub_socket.RCVTIMEO = 100000
self.sub_socket.setsockopt(zmq.SUBSCRIBE, b'')
self.sub_socket.setsockopt(zmq.LINGER, 0)
self.sub_socket.connect("tcp://"+self.host+":"+self.sub_port)
#loop here
while True:
	msg = self.sub_socket.recv()
	#do something with msg
```

You can check out the method `pollNewMessages` inside MetatraderConnector for reference.


