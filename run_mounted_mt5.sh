#!/bin/sh

#call me: ./run_mounted_mt5.sh account_id zmqtrader 132.19.0.2



#pass the account ID, which is then used to load the proper ini config.
#e.g. if you pass am_40693944,
#then am_40693944.ini is loaded.
#am_40693944.ini should be located inside the accounts/mt5 directory.

#template: set it to `zmqtrader` in order to runt he zmqtrader EA
ktnr=$1
template=$2
ipaddr=$3


#in order to start the container with name mt5test and to reach the vnc under <ipaddr>:5902


opt=$4 #add -d to run it in detached mode


echo "Using IP ADDR: $ipaddr"

#delete the stupid terminal ini
rm -f "$(pwd)/mt45/data/mt5/Config/terminal.ini"

echo "running docker"
#-d
docker run  $opt  --rm \
    --cap-add=SYS_PTRACE \
    --name ${PREFIX}mt5${ktnr} \
    --expose 5900 \
    --net mt_default \
    --ip $ipaddr \
    --log-driver json-file \
    --log-opt max-size=10m \
    --log-opt max-file=3 \
    -w /home/winer/.wine/drive_c/mt5 \
    -v "$(pwd)/mt45/accounts/mt5":/home/winer/.wine/drive_c/mt5/mounted_accounts:rw \
    -v "$(pwd)/mt45/data/mt5":/home/winer/.wine/drive_c/mt5:rw \
    -v "$(pwd)/mt45/run_mt5.sh":/docker/run_mt5.sh:rw \
    funkyhedge/mt5:latest \
    /docker/run_mt5.sh $ktnr $template

    #--network host \
#   -v "/etc/passwd":/etc/passwd:rw \
