
from cmath import exp
import dateutil.parser
import sys
sys.path.append("./")
sys.path.append("../")

import traceback as tb
import numpy as np
import logging
import copy
from datetime import timedelta
import datetime
import json

from Types import Candle,  Ticker, TradeSignal
from enum import Enum
from Types.Order import Order, State
import re
import zmq
import random
import sys
import time
from threading import Thread, Lock
import threading
sys.path.append("./")
sys.path.append("../")
from contextlib import contextmanager

class NoPllingDataError(Exception):
    """Base class for other exceptions"""
    pass


class Mt4OrderType(Enum):
    #see https://docs.mql4.com/constants/tradingconstants/orderproperties
    OP_BUY = 0
    OP_SELL = 1
    OP_BUYLIMIT = 2
    OP_SELLLIMIT = 3
    OP_BUYSTOP = 4
    OP_SELLSTOP = 5


class MetatraderConnector:

    def __init__(self, symbols, rep_port="32768",
              sub_port="32770", host="127.0.0.1", app=None, readonly=False, topicfilter=''):
        """
        rep_port: is the rep_port in the mq4 EA
        sub_port: is the PUB_PORT in the mq4 EA
        """
        #print("Current libzmq version is %s" % zmq.zmq_version())
        #print("Current  pyzmq version is %s" % zmq.pyzmq_version())
        self.rep_socket = None
        logging.info("Establishing MT connection to host "+str(host))
        self.rep_port = rep_port
        self.sub_port = sub_port
        self.app = app
        self.host = host
        self.symbols = [si.upper() for si in symbols]
        self.readonly = readonly
        self._stop_event = threading.Event()
        self.last_sucessful_polling_time = datetime.datetime.now()

        self.context = zmq.Context()

        #create data stream subscription
        logging.info("Setting up sub socket to "+self.host+":"+str(self.sub_port))
        self.sub_socket = self.context.socket(zmq.SUB)
        # in milliseconds. set it to a high value, since it depends on how often new prices arrive
        self.sub_socket.RCVTIMEO = 100000
        self.sub_socket.setsockopt(zmq.SUBSCRIBE, topicfilter.encode())
        #socket.setsockopt(zmq.SUBSCRIBE, topicfilter)
        self.sub_socket.setsockopt(zmq.LINGER, 0)
        self.sub_socket.connect("tcp://"+self.host+":"+self.sub_port)

        #now setup a thread, which polls for new data on the sub socket

        self.onBookMsgHandlers = []
        self.onTradeMsgHandlers = []
        self.onQuotesHandlers = []
        self.onTickerHandlers = []
        self.onCandleHandlers = []

        self.onTradeEventHandlers = []
        self.zmq_mutex = Lock()


    def setupReqRespSockets(self, timeout=5000):
        """
        connects to the push and pull sockets.
        make sure to lock the zmq_mutex before calling this! 
        """

        if self.readonly:
            raise Exception(
                "The MT connection was set to read only. You cant open up reqresp sockets")

        #logging.info("Setting up push socket to "+self.host+":"+str(self.rep_port))
        if self.rep_socket == None:
            #logging.info("Creating the push socket")
            self.rep_socket = self.context.socket(zmq.REQ)
        self.rep_socket.setsockopt(zmq.RCVTIMEO, timeout)
        self.rep_socket.setsockopt(zmq.SNDTIMEO, timeout)
        self.rep_socket.setsockopt(zmq.LINGER, 0)
        req_addr = "tcp://"+self.host+":"+self.rep_port
        #print("arg push: "+req_addr)
        self.rep_socket.connect(req_addr)

        #logging.info("Socket setup completed")

    def disconnectReqRespSockets(self):
        if self.rep_socket!=None:
            self.rep_socket.disconnect("tcp://"+self.host+":"+self.rep_port)
            self.rep_socket.close()
            self.rep_socket = None

    def addOnQuoteHandler(self, hi):
        self.onQuotesHandlers.append(hi)

    def addOnCandleHandler(self, hi):
        #do not provide the cabndle events yet
        pass

    def setSymbols(self, symbols=[]):
        if self.readonly:
            raise Exception(
                "The MT connector is running in READONLY mode. You need to call the constructor with different arguments if you want to write to MT")

        if len(symbols)>0:
            self.symbols=[si.upper() for si in symbols]
        msg = {'cmd': 'setSymbols', 'symbols': self.symbols}
        res = self.sendToMt(msg)
        if res == None:
            raise Exception("The response on setSymbols is not valid: "+str(res))
        logging.info("Setting symbols responded with: "+str(res))
        return res

    def addSymbol(self, symbol):
        if self.readonly:
            raise Exception(
                "The MT connector is running in READONLY mode. You need to call the constructor with different arguments if you want to write to MT")

        msg = {'cmd': 'addSymbol', 'symbol': symbol.upper()}
        self.symbols.append(symbol.upper())
        res = self.sendToMt(msg)
        if res == None:
            raise Exception("The response on setSymbols is not valid: "+str(res))
        logging.info("Adding symbol responded with: "+str(res))
        return res

    def getAccountInfo(self):
        msg = {'cmd': 'getAccountInfo'}
        try:
            res = self.sendToMt(msg)
        except Exception as e:
            logging.error(self.host+" Failed getting the account info"+str(e), exc_info=True)
            raise e
        if res == None:
            raise Exception("The response on "+self.host+".getAccountInfo is not valid: "+str(res))
        logging.info("getAccountInfo responded with: "+str(res))
        return res

    def run(self, interval_ms):
        self.polling = Thread(target=self.pollNewMessages, args=(interval_ms,))
        self.polling.start()

    def pollNewMessages(self, interval_ms):
        """
        checks if there is a new ticker msg or similar
        """
        logging.info("Started polling messages")
        while not self._stop_event.is_set():
            time.sleep(interval_ms/1000.0)
            try:
                logging.debug("Trying to poll a new msg from " +
                              str(self.host)+":"+str(self.sub_port))
                msg = self.sub_socket.recv()
                logging.debug("from "+str(self.host)+":" +
                              str(self.sub_port)+" received zMQ msg: "+str(msg))
                self.last_sucessful_polling_time = datetime.datetime.now()
                if len(msg) > 0:
                    self.onNewMessage(msg)
            except Exception as e:
                s = tb.format_exc()

                err_msg = "Error on polling zmq MT data from " + \
                    str(self.host)+":"+str(self.sub_port)+"::: "+str(e)
                logging.error(err_msg)
                logging.error(s)
                #raise NoPllingDataError(err_msg)
        logging.error("Should not have arrived here. Polling died?!")

    def onNewTradeEvent(self, msg):
        """
        is executed when MT registers an external trade event 
        triggered by some signal or so
        """
        logging.debug("Received trade event: "+str(msg))
        for ei in self.onTradeEventHandlers:
            ei(msg)

    def onNewMessage(self, msg):
        """
        creates a ticker and submits it to the trading app
        msg: string or jaons?
        """

        logging.debug("Received msg: "+str(msg))
        try:
            obj = json.loads(msg)
        except Exception as e:
            logging.error("Failed parsing json: "+str(e))
            msg = msg.decode("utf-8").replace("'", "__").replace("\"","'").replace("__", "\"")
            logging.debug("decoded msg with replaced  quotes: "+str(msg))
            try:
                obj = json.loads(msg)
                logging.debug("Received ticker: "+str(msg))
            except Exception as e:
                logging.error("Failed decoding "+str(msg))
                logging.error(e, exc_info=True)


        if "operation" in obj:
            self.onNewTradeEvent(obj)
            return

        dt = datetime.datetime.now()
        symbol = obj['symbol'].lower()
        ticker = Ticker.Ticker(dt,
                         symbol,
                         ask=float(obj['ask']),
                         bid=float(obj['bid']))
        ticker.server_time = int(obj['server_time'])
        ticker.terminal_time = int(obj['terminal_time'])

        for hi in self.onTickerHandlers:
            hi(ticker)

        #need to comply with the recorder message format:
        #{"timestamp": "2019-11-01T00:00:06.572000", "data": {"table": "quote", "action": "insert",
        #"data": [{"timestamp": "2019-11-01T00:00:06.572000", "table": "quote", "symbol": "EURUSD",
        #"bidSize": 0.0, "bidPrice": 1.1163800000000001, "askPrice": 1.11644}]}}
        msg_btx = {'table': 'quote', 'action': 'insert',
                    'data': [{'timestamp': dt, 'table': 'quote', 'symbol': symbol,
                              'bidPrice': ticker.bid, 'askPrice': ticker.ask}]}
        for hi in self.onQuotesHandlers:
            hi(msg_btx)

    def kill(self):
        self._stop_event.set()

    def placeOrder(self, order: Order):
        """
        places an order.
        returns: position_id
        """
        self.checkReadonly()
        magic_num = 0
        if order.id != None:
            magic_num = int(order.id)

        sl = order.sl #0.0
        tp = order.tp #0.0



        order_msg = {'symbol': order.symbol.upper(), 'type': self.order2Mt4Type(order), 'lots': float(np.abs(order.qty)),
                    'price': float(order.price), 'SL': sl, 'TP': tp, 'comment': order.descr, 'magic': 0,
                    'max_slippage': 2}
        msg = {'cmd': 'placeOrder', 'order': order_msg}
        #logging.info("sending to mt@"+self.host+": "+str(msg))
        try:
            res = self.sendToMt(msg)
            if res == None:
                raise Exception("The response of placeOrder is not valid: "+str(res))
        except Exception as e:
            logging.error("Failed to place the order: "+str(e))
            raise e

        logging.info("res: "+str(res))
        if res['state'] == "ok":
            copy_order = copy.deepcopy(order)

            copy_order.exchange_uid = res['order_id']
            copy_order.price = res['open_price']
            copy_order.placed = True
            copy_order.filled = True
            copy_order.filled_prc = 100.0
            copy_order.filled_qty = copy_order.qty
            copy_order.filling_time = datetime.datetime.now()
            return copy_order
        else:
            raise Exception("Could not place the order "+str(order_msg)+", "+str(res))

    def cancelOrder(self, order):
        raise Exception("cancelOrder is not implemented")

    def getOpenPositions(self):
        try:
            msg = {'cmd': 'getOpenOrders'}
            res = self.sendToMt(msg)
            #logging.info("getOpenPositions returned: "+str(res))
            if res == None:
                raise Exception("Could not call getOpenOrders")
            return res
        except Exception as e:
            logging.error("Could not get open positions: "+str(e))
            raise e

    def closePosition(self, order, max_slippage=2):
        """
        closes the order by market.
        position_id: returned by MT when placing an order.
        """
        self.checkReadonly()
        msg = {'cmd': 'closePosition', 'position': {'order_id': int(
            order.exchange_uid), 'slippage': max_slippage, 'qty': float(np.abs(order.qty))}}
        logging.info("sending to mt: "+str(msg))
        res = self.sendToMt(msg)
        logging.info("closePosition res: "+str(res))
        if res == None:
            raise Exception("The MT zmq server did not respond!")
        #TODO: Need to get details such as slippage

        if res['state'] == "ok":
            out_order = copy.deepcopy(order)
            out_order.price = res['exit_price']
            return out_order
        else:

            logging.error("MT responded with res: "+str(res)+", order: "+str(order), extra={'res':res, 'order':order})
            raise Exception("Could not close the position. "+str(res))
            #return None  # return res['state']=="ok"

    def getPastTicks(self, symbol: str, start_time: datetime = None, num: int = 10000):
        """
        download historical tick data
        """
        if start_time != None:
            unix_time = int(start_time.timestamp()*1e3)
        else:
            unix_time = 0
        msg = {'cmd': 'getPastTicks', 'symbol': symbol,
                    'num': num, 'start_time': unix_time}
        logging.info("Sending to mt: "+str(msg))
        res = self.sendToMt(msg)
        if res == None:
            raise Exception("Could not receive anything from MT (getPastTicks)")
        #note, the array has the inverse order
        ticks = []
        print(res[0])
        for ti in res:
            time = datetime.datetime.fromtimestamp(
                float(ti['time'])/1000.0)-timedelta(hours=1)
            tick = Ticker.Ticker(time, symbol, ti['ask'], ti['bid'])
            tick.vol = ti['volume']
            ticks.append(tick)
        ticks.reverse()
        print("tick[-1]: "+str(ticks[-1]))
        print("tick[0]: "+str(ticks[0]))

        return ticks

    def getPastCandles(self, symbol: str, timeframe: str, num: int, start_time=None, end_time=None):
        """
        returns num past candles. [0] is the current one.
        timeframe: 1m, 5m, 15m, 30m, 1H, 4H, 1d
        start and end time: use this format : yyyy.mm.dd [hh:mi]"
            https://www.mql5.com/en/docs/convert/stringtotime
        """

        msg = {'cmd': 'getPastCandles', 'symbol': symbol.upper(), 'num': num,
                    'timeframe': timeframe}

        if start_time != None and end_time != None:
            msg = {'cmd': 'getPastCandlesRange', 'symbol': symbol.upper(
            ), 'start_time': start_time, 'end_time': end_time, 'timeframe': timeframe}

        logging.info("sending to mt: "+str(msg))
        res = self.sendToMt(msg, timeout=200000)
        logging.debug("getPastCandles res: "+str(res))
        #print(res)
        if res == None:
            raise Exception("The MT zmq server did not respond!")
        if type(res) == dict and res["state"] == "error":
            raise Exception(res["msg"])

        #parse the candles
        #parse interval
        interval_mins = 0
        num = int(re.findall("[0-9]+", timeframe)[0])
        if "m" in timeframe:
            interval_mins = num
        if "h" in timeframe:
            interval_mins = num*60
        if "d" in timeframe:
            interval_mins = num*60*24

        candles = []

        for ri in res:
            ci = Candle.Candle(symbol.lower())
            ci.parseFromMt(ri, interval_mins)
            candles.append(ci)

        candles.reverse()

        #print("candle[-1]: "+str(candles[-1]))
        #print("candle[0]: "+str(candles[0]))

        for hi in self.onCandleHandlers:
            for ci in candles:
                hi(self.candle2msg(ci))

        return candles  # todo parse them

    def candle2msg(self, candle):
        """
        converts a candle to a (bitmex) json format
        """
        tf = str(candle.interval)
        msg = {'table': 'tradeBin'+tf+'m', 'action': 'insert'}

        sample_time = candle.start_time.strftime("%Y-%m-%dT%H:%M:%S")
        item_data = {'timestamp': sample_time, 'symbol': candle.symbol,
                    'open': candle.opn, 'high': candle.cmax, 'low': candle.cmin, 'close': candle.cls,
                    'trades': 1, 'volume': candle.vol}
        msg['data'] = [item_data]
        return msg

    def subscribeSignal(self, signal_id):
        """
        subscribes to a MQL-Market signal
        """
        msg = {'cmd': 'subscribeSignal', 'signal_id': str(signal_id)}
        res = self.sendToMt(msg)
        if res == None:
            raise Exception("Could not subscribe to the signal " +
                            str(signal_id)+". Error unknown")
        if res['state'] != "ok":
            raise Exception("Failed subscribing to a signal " +
                            str(signal_id)+". Error Code: "+str(res['error']))
        return True

    def updatePosition(self, exchange_uid, sl_price, tp_price):
        """
        sends the order to MT and updates its SL and TP
        """
        self.checkReadonly()
        msg = {'cmd': 'updateOrder', 'position': {'order_id': int(
            exchange_uid), 'sl': round(sl_price,6), 'tp': round(tp_price,6)}}
        res = self.sendToMt(msg)

        if res == None:
            raise Exception("The MT4 zmq server did not respond!")

        if not res['state'] == "ok":
            logging.error(str(res))
            print(res)
            raise Exception("Error in MT: "+str(res['msg']))

        return res['state'] == "ok"

    def checkReadonly(self):
        if self.readonly:
            raise Exception(
                "The MT connector is running in READONLY mode. You need to call the constructor with different arguments if you want to write to MT")

    def parseResponse(self, resp):
        res=resp.decode("utf-8")#.replace("§", "?")
        res_switched_brackets = res.replace("'", "__").replace("\"", "'").replace("__", "\"")
        try:
            out = json.loads(res_switched_brackets)
            return out
        except Exception as e:
            try:
                #logging.info(f"trying to decode {res}")
                out = json.loads(res)
                return out
            except Exception as e:
                logging.error(tb.format_exc()) #what happens here??
                raise Exception("Could not parse the returned string to json: " +
                                str(res_switched_brackets)+", "+str(e))

    
    def sendToMt(self, msg, timeout=5000):
        #logging.info("Sending msg to MT: "+str(msg))
        out=None

        try:
            #logging.info("acquire mutex")
            self.zmq_mutex.acquire(timeout=timeout)
        except Exception as e:
            logging.error("Failed acquiring the zmq mutex", exc_info=True)
            raise e

        def _cleanup():
            try:
                self.disconnectReqRespSockets() 
            finally:
                self.zmq_mutex.release()
        
        try:
            #logging.info("setup socket")
            self.setupReqRespSockets(timeout=timeout)
        except Exception as e:
            logging.error("Failed setting up zmq socket:"+str(e), exc_info=True)
            _cleanup()
            raise e
        
        try:
            #logging.info("send json cmd "+str(msg))
            self.rep_socket.send_json(msg, flags=0)
        except Exception as e:
            logging.error("Failed sending data to MT: "+str(e), exc_info=True)
            _cleanup()
            raise e
            
        try:
            #logging.info("getting response")
            res = self.rep_socket.recv()
        except Exception as e:
            logging.error("Failing receiving data from MT: "+str(e), exc_info=True)
            _cleanup()
            raise e
        
        _cleanup()
        
        try:
            #logging.info("parsing response")
            out = self.parseResponse(res)
        except Exception as e:
            logging.error("Failed parsing response from mt: "+str(res), exc_info=True)
            raise e

        return out

    def order2Mt4Type(self, order):
        """
        returns a MetaTrader type from the order action/state
        See https://www.mql5.com/de/docs/constants/tradingconstants/orderproperties#enum_order_type
        for reference!
        """
        if order.action == "BUY":
            return Mt4OrderType.OP_BUY.value #0
        if order.action == "SELL":
            return Mt4OrderType.OP_SELL.value #1
        if order.action== "BUY_LIMIT":
            return 2
        if order.action == "SELL_LIMIT":
            return 3
        if order.action == "BUY_STOP":
            return 4
        if order.action == "SELL_STOP":
            return 5


        raise Exception("The action "+order.action+" is not supported for MT")

    #(dummy) methods to comply with the StreamRecorder
    def addOnQuoteHandler(self, handler):
        self.onQuotesHandlers.append(handler)

    def addOnTradeBinHandler(self, handler):
        pass

    def getCandles(self, symbol, interval):
        raise Exception("getCandles is not possible with the MetatraderConnector")


def test_placeorder(symbol, mt, golong=True):

    if golong:
        #buy first, wait 20 seconds and close the order
        order = Order(symbol, action="BUY", order_type="-", price=0, qty=0.01)
        order.descr="MT5 test long"

        order.handling_event = order.handling_event = TradeSignal.TradeSignal.BUY_OPEN
        opened_order = mt.placeOrder(order)

        print("placeOrder returned: "+str(opened_order))
        print("waiting 20 secs before closing the position")

        time.sleep(10)
        close_order=Order(symbol, action="CLOSE", price=0, qty=0)
        close_order.exchange_uid = opened_order.exchange_uid
        res = mt.closePosition(close_order, max_slippage=2)
        print("Closing position returned: "+str(res))
    else:
        print("Setting SHORT order")
        order = Order(symbol, action="SELL", order_type="-", price=0, qty=0.01)
        order.descr = "MT5 test short"
        order.handling_event = TradeSignal.TradeSignal.SELL_OPEN
        opened_order = mt.placeOrder(order)
        print("placeOrder returned: "+str(opened_order))
        print("waiting 20 secs before closing the position")

        time.sleep(10)
        close_order = Order(symbol, action="CLOSE", price=0, qty=0)
        close_order.exchange_uid = opened_order.exchange_uid
        res = mt.closePosition(close_order, max_slippage=2)
        print("Closing position returned: "+str(res))




def test_placeorder_limit(symbol, mt, golong=True):

    if golong:
        curr_ticks = mt.getPastTicks(symbol=symbol, num=2)
        limit_price= curr_ticks[-1].midPrice() - curr_ticks[-1].midPrice()*0.1/100

        #buy first, wait 20 seconds and close the order
        order = Order(symbol, action="BUY_LIMIT", order_type="-", price=limit_price, qty=0.01)

        order.handling_event = order.handling_event = TradeSignal.TradeSignal.BUY_OPEN
        opened_order = mt.placeOrder(order)

        print("placeOrder returned: "+str(opened_order))
        print("waiting 20 secs before closing the position")

        time.sleep(20)
        res = mt.closePosition(opened_order, max_slippage=2)
        print("Closing position returned: "+str(res))
    else:
        print("Setting SHORT LIMIT order")
        curr_ticks=mt.getPastTicks(symbol=symbol, num=2)
        limit_price= curr_ticks[-1].midPrice() + curr_ticks[-1].midPrice()*0.1/100
        sl=limit_price + limit_price*0.2/100
        tp=limit_price - limit_price* 0.2/100
        

        order = Order(symbol, action="SELL_LIMIT", order_type="-", price=limit_price, qty=0.01)
        order.tp = tp
        order.sl = sl
        order.descr = "MT5 test short"
        order.handling_event = TradeSignal.TradeSignal.SELL_OPEN
        opened_order = mt.placeOrder(order)
        print("placeOrder returned: "+str(opened_order))
        print("waiting 20 secs before closing the position")

        time.sleep(10)
        res = mt.closePosition(opened_order, max_slippage=2)
        print("Closing position returned: "+str(res))


def test_full():

    symbol = "GBPUSD"
    symbol2 = "EURUSD"
    logging.info("Establishing connection...")
    host = "localhost"
    if len(sys.argv) > 1:
        host = sys.argv[1]

    mt = MetatraderConnector(symbols=[symbol, symbol2], host=host)
    mt.run(interval_ms=50)
    test_placeorder(symbol2, mt)

    res = mt.setSymbols()
    logging.info("Setting symbols returned: "+str(res))
    #sys.exit(0)

    #test open position reading
    positions = mt.getOpenPositions()
    logging.info("open positions: "+str(positions))

    #test update TP and SL
    for oi in positions:
        order = Order(oi["symbol"], action=oi["type"], order_type="-",
                      price=oi["open_price"], qty=oi["qty"])
        order.exchange_uid = oi["id"]
        #logging.info("Synchronizing position "+str(order))
        old_tp = order.tp
        old_sl = order.sl
        order.tp = 1000000
        order.sl = 0.2
        if order.action == "SELL":
            order.tp = 0.2
            order.sl = 10000.0
        print("Updating order with "+str(order))
        mt.updatePosition(order)
        time.sleep(3)

        #set back
        order.tp = old_tp
        order.sl = old_sl
        print("Setting back order to "+str(order))
        mt.updatePosition(order)
        break

    print("Get open orders and modify orders tested.")
    #sys.exit(0)
    #while True:
    #	time.sleep(1)

    def onMsg(ticker):
        logging.info("New ticker: "+str(ticker))
    mt.onTickerHandlers.append(onMsg)
    res = mt.setSymbols()
    logging.info("Setting symbols returned: "+str(res))
    mt.run(interval_ms=50)
    time.sleep(10)
    print("Tested ticker data subscription")
    #sys.exit(-1)
    #raise Exception("ende")

    #---------------------------------
    #res=mt.setSymbols()
    logging.info("Setting symbols returned: "+str(res))
    mt.run(interval_ms=50)

    test_placeorder(symbol2, mt)

    mt.kill()
    #mt.run(interval_ms=10)


def test_listener():
    host = "172.19.0.5"
    if len(sys.argv) > 1:
        host = sys.argv[1]

    mt = MetatraderConnector(
        symbols=["EURUSD", "GBPUSD", "USDJPY"], host=host, readonly=False)
    mt.setSymbols()

    def onMsg(ticker):
        logging.info("New ticker: "+str(ticker))
    mt.onTickerHandlers.append(onMsg)
    mt.run(interval_ms=50)


def getPastCandles():
    host = "localhost"
    if len(sys.argv) > 1:
        host = sys.argv[1]

    symbols = ["EURUSD"]
    mt = MetatraderConnector(symbols=symbols, host=host)
    candles = mt.getPastCandles(symbols[0], 1, 20)
    #for ci in candles:
    #	print(str(ci))


def testPastTicks():
    host = "localhost"
    if len(sys.argv) > 1:
        host = sys.argv[1]

    symbols = ["EURUSD"]
    mt = MetatraderConnector(symbols=symbols, host=host)
    # datetime.datetime.now()-timedelta(days=30), 0)
    ticks = mt.getPastTicks(symbols[0], None, 3000)
    print("received "+str(len(ticks)))
    for ti in ticks[:10]:
        print(str(ti))


def test_only_order_placement():
    host = "172.19.0.5"
    if len(sys.argv) > 1:
        host = sys.argv[1]

    symbols = ["GBPUSD", "EURUSD"]

    mt = MetatraderConnector(symbols=symbols, host=host)
    logging.info("Setting symbols first")
    #mt.setSymbols()
    #mt.run(interval_ms=50)

    test_placeorder(symbols[0], mt, golong=False)
    test_placeorder(symbols[0], mt, golong=True)

    #test_placeorder_limit(symbols[0], mt, golong=False)
    #test_placeorder_limit(symbols[0], mt, golong=True)

    


def test_signal_subscription(host):
    """
    This tests involves actually the zmq_republisher and not the zmq_trader!
    """
    logging.info("Connecting to MT host "+str(host))
    mt = MetatraderConnector(symbols=["EURUSD"], host=host)
    #signal_id="956199" #some free signal
    signal_id = "926390"  # PowerBot
    logging.info("Trying to subscribe to a signal")
    res = mt.subscribeSignal(signal_id)
    logging.info("Result: "+str(res))


def test_account_info(host):

    def getAcc(host, thrNum):
        for i in range(10):
            time.sleep(0.1)
            mt = MetatraderConnector(symbols=["EURUSD"], host=host)
            res = mt.getAccountInfo()
            print("from "+str(thrNum)+": "+str(res))

    #multi threading for load testing
    for i in range(10):
        thr = Thread(target=getAcc, args=(host, i,))
        thr.start()


if __name__ == "__main__":
    # '(%(threadName)s) %(asctime)s.%(msecs)03d %(levelname)s %(filename)s:%(lineno)d %(message)s'
    recfmt = "%(threadName)s || %(levelname)s: %(asctime)s.%(msecs)03d - [%(filename)s:%(lineno)s:%(funcName)s ] %(message)s"
    timefmt = '%y%m%d_%H:%M:%S'
    logging.basicConfig(level=logging.DEBUG,
                     format=recfmt, datefmt=timefmt)
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--host",  default="172.19.0.2",
                        help="Host name of the MT terminal")

    
    args = parser.parse_args()


    test_account_info(args.host)

    #test_full()
    #logging.info("Starting listener!")
    #test_listener()
    #test_only_order_placement()



    #getPastCandles()

    #test_only_order_placement()
    #testPastTicks()
