
from cmath import exp
import dateutil.parser
import sys
sys.path.append("./")
sys.path.append("../")

import traceback as tb
import numpy as np
import logging
import copy
from datetime import timedelta
import datetime
import json

from Types import Candle,  Ticker, TradeSignal
from enum import Enum
from Types.Order import Order, State
import time
from threading import Thread, Lock
import threading

import argparse
import MetatraderConnector

import matplotlib.pyplot as plt

def test_time_offset_ticker(host, num_to_collect = 100):
    """
    subscribes to tick data and compares the time stamps
    """
    
    mt = MetatraderConnector.MetatraderConnector(
        symbols=["EURUSD"], host=host, readonly=False, topicfilter="")
    res = mt.setSymbols()
    logging.info("Set symbols returned: "+str(res))

    tickers =[]
    def onMsg(ticker):
        logging.info("New ticker: "+str(ticker))
        tickers.append(ticker)
    mt.onTickerHandlers.append(onMsg)
    mt.run(interval_ms=0)

    while len(tickers) < num_to_collect:
        logging.info("Collected "+str(len(tickers))+" ticks")
        time.sleep(1)

    diffs = [ti.terminal_time - ti.server_time for ti in tickers]
    plt.hist(diffs, bins=30)
    plt.title("Difference of server time and terminal time")
    plt.grid()
    plt.show()



if __name__ == "__main__":
    # '(%(threadName)s) %(asctime)s.%(msecs)03d %(levelname)s %(filename)s:%(lineno)d %(message)s'
    recfmt = "%(threadName)s || %(levelname)s: %(asctime)s.%(msecs)03d - [%(filename)s:%(lineno)s:%(funcName)s ] %(message)s"
    timefmt = '%y%m%d_%H:%M:%S'
    logging.basicConfig(level=logging.DEBUG,
                     format=recfmt, datefmt=timefmt)
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--host",  default="132.19.0.2",
                        help="Host name of the MT terminal")
    parser.add_argument("--n",  default=10,
                        help="Amount of ticks to collect before plotting")

    
    args = parser.parse_args()


    test_time_offset_ticker(host = args.host, num_to_collect=int(args.n))