
from cmath import exp
import dateutil.parser
import sys
sys.path.append("./")
sys.path.append("../")

import traceback as tb
import numpy as np
import logging
import copy
from datetime import timedelta
import datetime
import json

from Types import Candle,  Ticker, TradeSignal
from enum import Enum
from Types.Order import Order, State
import time
from threading import Thread, Lock
import threading

import argparse
import MetatraderConnector


def test_placeorder(symbol, mt, golong=True):

    if golong:
        #buy first, wait 20 seconds and close the order
        order = Order(symbol, action="BUY", order_type="-", price=0, qty=0.01)
        order.descr="MT5 test long"

        order.handling_event = order.handling_event = TradeSignal.TradeSignal.BUY_OPEN
        opened_order = mt.placeOrder(order)

        print("placeOrder returned: "+str(opened_order))
        print("waiting 20 secs before closing the position")

        time.sleep(10)
        close_order=Order(symbol, action="CLOSE", price=0, qty=0)
        close_order.exchange_uid = opened_order.exchange_uid
        res = mt.closePosition(close_order, max_slippage=2)
        print("Closing position returned: "+str(res))
    else:
        print("Setting SHORT order")
        order = Order(symbol, action="SELL", order_type="-", price=0, qty=0.01)
        order.descr = "MT5 test short"
        order.handling_event = TradeSignal.TradeSignal.SELL_OPEN
        opened_order = mt.placeOrder(order)
        print("placeOrder returned: "+str(opened_order))
        print("waiting 20 secs before closing the position")

        time.sleep(10)
        close_order = Order(symbol, action="CLOSE", price=0, qty=0)
        close_order.exchange_uid = opened_order.exchange_uid
        res = mt.closePosition(close_order, max_slippage=2)
        print("Closing position returned: "+str(res))




def test_placeorder_limit(symbol, mt, golong=True):

    if golong:
        curr_ticks = mt.getPastTicks(symbol=symbol, num=2)
        limit_price= curr_ticks[-1].midPrice() - curr_ticks[-1].midPrice()*0.1/100

        #buy first, wait 20 seconds and close the order
        order = Order(symbol, action="BUY_LIMIT", order_type="-", price=limit_price, qty=0.01)

        order.handling_event = order.handling_event = TradeSignal.TradeSignal.BUY_OPEN
        opened_order = mt.placeOrder(order)

        print("placeOrder returned: "+str(opened_order))
        print("waiting 20 secs before closing the position")

        time.sleep(20)
        res = mt.closePosition(opened_order, max_slippage=2)
        print("Closing position returned: "+str(res))
    else:
        print("Setting SHORT LIMIT order")
        curr_ticks=mt.getPastTicks(symbol=symbol, num=2)
        limit_price= curr_ticks[-1].midPrice() + curr_ticks[-1].midPrice()*0.1/100
        sl=limit_price + limit_price*0.2/100
        tp=limit_price - limit_price* 0.2/100
        

        order = Order(symbol, action="SELL_LIMIT", order_type="-", price=limit_price, qty=0.01)
        order.tp = tp
        order.sl = sl
        order.descr = "MT5 test short"
        order.handling_event = TradeSignal.TradeSignal.SELL_OPEN
        opened_order = mt.placeOrder(order)
        print("placeOrder returned: "+str(opened_order))
        print("waiting 20 secs before closing the position")

        time.sleep(10)
        res = mt.closePosition(opened_order, max_slippage=2)
        print("Closing position returned: "+str(res))


def test_full():

    symbol = "GBPUSD"
    symbol2 = "EURUSD"
    logging.info("Establishing connection...")
    host = "localhost"
    if len(sys.argv) > 1:
        host = sys.argv[1]

    mt = MetatraderConnector.MetatraderConnector(symbols=[symbol, symbol2], host=host)
    mt.run(interval_ms=50)
    test_placeorder(symbol2, mt)

    res = mt.setSymbols()
    logging.info("Setting symbols returned: "+str(res))
    #sys.exit(0)

    #test open position reading
    positions = mt.getOpenPositions()
    logging.info("open positions: "+str(positions))

    #test update TP and SL
    for oi in positions:
        order = Order(oi["symbol"], action=oi["type"], order_type="-",
                      price=oi["open_price"], qty=oi["qty"])
        order.exchange_uid = oi["id"]
        #logging.info("Synchronizing position "+str(order))
        old_tp = order.tp
        old_sl = order.sl
        order.tp = 1000000
        order.sl = 0.2
        if order.action == "SELL":
            order.tp = 0.2
            order.sl = 10000.0
        print("Updating order with "+str(order))
        mt.updatePosition(order)
        time.sleep(3)

        #set back
        order.tp = old_tp
        order.sl = old_sl
        print("Setting back order to "+str(order))
        mt.updatePosition(order)
        break

    print("Get open orders and modify orders tested.")
    #sys.exit(0)
    #while True:
    #	time.sleep(1)

    def onMsg(ticker):
        logging.info("New ticker: "+str(ticker))
    mt.onTickerHandlers.append(onMsg)
    res = mt.setSymbols()
    logging.info("Setting symbols returned: "+str(res))
    mt.run(interval_ms=50)
    time.sleep(10)
    print("Tested ticker data subscription")
    #sys.exit(-1)
    #raise Exception("ende")

    #---------------------------------
    #res=mt.setSymbols()
    logging.info("Setting symbols returned: "+str(res))
    mt.run(interval_ms=50)

    test_placeorder(symbol2, mt)

    mt.kill()
    #mt.run(interval_ms=10)


def test_listener(host):
    
    if len(sys.argv) > 1:
        host = sys.argv[1]

    mt = MetatraderConnector.MetatraderConnector(
        symbols=["EURUSD"], host=host, readonly=False)
    mt.setSymbols()

    def onMsg(ticker):
        logging.info("New ticker: "+str(ticker))
    mt.onTickerHandlers.append(onMsg)
    mt.run(interval_ms=0)




def test_time_offset_ticker(host):
    """
    subscribes to tick data and compares the time stamps
    """
    
    mt = MetatraderConnector.MetatraderConnector(
        symbols=["EURUSD"], host=host, readonly=False)
    mt.setSymbols()

    tickers =[]
    def onMsg(ticker):
        logging.info("New ticker: "+str(ticker))
        tickers.append(ticker)
    mt.onTickerHandlers.append(onMsg)
    mt.run(interval_ms=0)

    while len(tickers) <100:
        logging.info("Collected "+str(len(tickers))+" ticks")
        time.sleep(1)
        
    import matplotlib.pyplot as plt

    diffs = [ti.terminal_time - ti.server_time for ti in tickers]
    plt.hist(diffs, bin=30)
    plt.title("Difference of server time and terminal time")
    plt.grid()



def getPastCandles():
    host = "localhost"
    if len(sys.argv) > 1:
        host = sys.argv[1]

    symbols = ["EURUSD"]
    mt = MetatraderConnector.MetatraderConnector(symbols=symbols, host=host)
    candles = mt.getPastCandles(symbols[0], 1, 20)
    #for ci in candles:
    #	print(str(ci))


def testPastTicks():
    host = "localhost"
    if len(sys.argv) > 1:
        host = sys.argv[1]

    symbols = ["EURUSD"]
    mt = MetatraderConnector.MetatraderConnector(symbols=symbols, host=host)
    # datetime.datetime.now()-timedelta(days=30), 0)
    ticks = mt.getPastTicks(symbols[0], None, 3000)
    print("received "+str(len(ticks)))
    for ti in ticks[:10]:
        print(str(ti))


def test_only_order_placement():
    host = "172.19.0.5"
    if len(sys.argv) > 1:
        host = sys.argv[1]

    symbols = ["GBPUSD", "EURUSD"]

    mt = MetatraderConnector.MetatraderConnector(symbols=symbols, host=host)
    logging.info("Setting symbols first")
    #mt.setSymbols()
    #mt.run(interval_ms=50)

    test_placeorder(symbols[0], mt, golong=False)
    test_placeorder(symbols[0], mt, golong=True)

    #test_placeorder_limit(symbols[0], mt, golong=False)
    #test_placeorder_limit(symbols[0], mt, golong=True)

    


def test_signal_subscription(host):
    """
    This tests involves actually the zmq_republisher and not the zmq_trader!
    """
    logging.info("Connecting to MT host "+str(host))
    mt = MetatraderConnector.MetatraderConnector(symbols=["EURUSD"], host=host)
    #signal_id="956199" #some free signal
    signal_id = "926390"  # PowerBot
    logging.info("Trying to subscribe to a signal")
    res = mt.subscribeSignal(signal_id)
    logging.info("Result: "+str(res))


def test_account_info(host):

    def getAcc(host, thrNum):
        for i in range(10):
            time.sleep(0.1)
            mt = MetatraderConnector.MetatraderConnector(symbols=["EURUSD"], host=host)
            res = mt.getAccountInfo()
            print("from "+str(thrNum)+": "+str(res))

    #multi threading for load testing
    for i in range(10):
        thr = Thread(target=getAcc, args=(host, i,))
        thr.start()


if __name__ == "__main__":
    # '(%(threadName)s) %(asctime)s.%(msecs)03d %(levelname)s %(filename)s:%(lineno)d %(message)s'
    recfmt = "%(threadName)s || %(levelname)s: %(asctime)s.%(msecs)03d - [%(filename)s:%(lineno)s:%(funcName)s ] %(message)s"
    timefmt = '%y%m%d_%H:%M:%S'
    logging.basicConfig(level=logging.DEBUG,
                     format=recfmt, datefmt=timefmt)
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--host",  default="132.19.0.2",
                        help="Host name of the MT terminal")

    
    args = parser.parse_args()


    #test_account_info(args.host)
    #test_listener(host = args.host)
    test_time_offset_ticker(host = args.host)

    #test_full()
    #logging.info("Starting listener!")
    #test_listener()
    #test_only_order_placement()



    #getPastCandles()

    #test_only_order_placement()
    #testPastTicks()
