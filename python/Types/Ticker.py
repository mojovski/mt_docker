"""
This class declares a ticker item which is a current response for a symbol/market at bittrex.
"""
import numpy as np


class Ticker:
    def __init__(self, timestamp, symbol, ask, bid, last=0):
        self.timestamp=timestamp
        self.ask=ask
        self.bid=bid
        self.last=0.0
        self.symbol=symbol.lower()
        self.vol=0
        self.server_time =0
        self.terminal_time =0


    def validate(self):
        if (self.ask == 0.0):
            raise Exception("Ticker:init. ask is zero")
        if (self.bid == 0.0):
            raise Exception("Ticker:init. bid is zero")

    def asList(self):
        out=[self.symbol, self.timestamp, self.ask, self.bid]
        return out

    def asNDArray(self):
        return np.array(self.asList()[2:4])

    def asDict(self):
        return {'ask':self.ask, 'bid':self.bid, 'time':self.timestamp, 'last':self.last, 'symbol':self.symbol}

    def __str__(self):
        return "Ticker "+self.symbol+": time:"+str(self.timestamp)+", ask: "+str(self.ask)+", bid: "+str(self.bid)

    def midPrice(self):
        return self.bid+(self.ask-self.bid)/2

    def spread(self):
        return self.ask-self.bid
        

    def __add__(self, other):
        """

        :param other: other ticker?
        :return:
        """
        out = Ticker(self.timestamp, self.symbol, self.ask, self.bid)
        out.ask += other.ask
        out.bid += other.bid

        return out

    def __sub__(self, other):
        """

        :param other: other ticker
        :return:
        """
        out = Ticker(self.timestamp, self.symbol, self.ask, self.bid)
        out.ask -= other.ask
        out.bid -= other.bid

        return out

    def __truediv__(self, other):
        """

        :param other: other ticker
        :return:
        """
        out = Ticker(self.timestamp, self.symbol, self.ask, self.bid)
        out.ask /= other.ask
        out.bid /= other.bid

        return out
