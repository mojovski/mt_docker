import time
import datetime
import threading
import logging
from enum import Enum
from Types import TimeWall

class State(Enum):
    NOSTATE=0
    TOBUY=1
    BOUGHT=2
    TOSELL=3
    SOLD=4
    HOLD=5
    TOLOSSSELL=6
    SOLDLOSS=7,
    TOCLOSE=8

class Order:
    def __init__(self, symbol, action, type, price, qty, good_after=None, good_before=None):
        self.type = type.upper()
        self.qty=qty
        self.bitmex_qty=qty #int(qty*price)

        self.symbol = symbol
        self.action=action.upper() #buy, sell
        self.handling_event=None #e.g. TradeSignal.SELL_OPEN
        self.price = price
        self.good_after = good_after
        self.good_before = good_before
        self.placed = False
        self.filled = False
        self.filled_prc=0.0
        self.sell_loss_prc=0.99
        self.sell_gain_prc=1.01
        self.is_bracket_order=False
        self.descr=""
        self.orig_time="" #used to remember the time of a ticker in simulated mode
        self.do_adaptive_gain_sell=False
        self.do_adaptive_loss_sell=False
        self.uid=None #update this from the bittrex api
        self.exchange_uid=None
        self.id=None
        self.onFilledCallbacks=[]
        self.prev_order=None #in case this is a gain sell or loss sell, this refers to the initial buy order.
        self.is_in_progress=False
        self.cancelled=False
        self.placement_time=TimeWall.timewall.getCurrentTime()
        self.qty_precision=7
        self.price_precision=7
        self.filled_qty=0
        self.state=State.NOSTATE
        self.filling_time=None
        self.sl=0 #stop loss
        self.tp=0
        self.timeout=int(1e9)

    def markPlaced(self):
        self.placed=True

    def markCancelled(self):
        self.cancelled=True
        self.is_in_progress=False

    def markFilled(self):
        self.filling_time = TimeWall.timewall.getCurrentTime() #Timewal.datetime.datetime.now()
        self.filled = True
        self.filled_qty = self.qty
        self.filled_prc=1.0

    def asDict(self):
        return self.__dict__


    def __str__(self):
        d = self.__dict__
        return str(d)
