from enum import auto, Enum

class TradeSignal(Enum):
    SELL_OPEN = auto()
    SELL_CLOSE = auto()
    BUY_OPEN = auto()
    BUY_CLOSE = auto()
    UPDATE = auto()