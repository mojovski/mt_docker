"""
This class declares a candle item with min, max, open, close, volume
"""
import numpy as np
import datetime
import dateutil.parser
from datetime import timezone
import logging
import dateutil

import pytz
local_timezone=pytz.timezone('Europe/Berlin')

class Candle:
    def __init__(self, symbol):
        self.symbol=symbol
        self.ts=0

        self.opn=0
        self.cls=0
        self.cmin=0
        self.cmax=0
        self.vol=0
        self.interval=0
        self.closed=False
        self.timestamp=0
        self.start_time=datetime.datetime.now()


    def setMembers(self, params):
        """
        uses the passed dict params to set the local vars
        """
        for k, v in params.items():
            #locals()[k]=v
            setattr(self, k, v)


    def parseFromPandasCsv(self, row, interval):
        """
        assume we can access the Candle data by row.opn, row.cls, row.min, row.max, row.vol
        """
        self.timestamp=row.timestamp
        self.start_time=self.timestamp-datetime.timedelta(minutes=interval)
        self.opn=row.opn
        self.cls=row.cls
        self.cmin=row.cmin
        self.cmax=row.cmax
        self.vol=row.vol
        self.interval=interval
        self.closed=True

    def parseFromAlphaVantage(self, row, interval):
        """
        assume we can access the Candle data by row.opn, row.cls, row.min, row.max, row.vol
        """
        #print("row: "+str(row))
        self.timestamp=row['date']
        self.start_time=self.timestamp #row['date']
        self.opn=row['open']
        self.cls=row['close']
        self.cmin=row['low']
        self.cmax=row['high']
        self.vol=row['volume']
        self.interval=interval
        self.closed=True

    def parseFromTiingo(self, row, interval):
        """
        assume we can access the Candle data by row.opn, row.cls, row.min, row.max, row.vol
        """
        #print(row['date'])
        self.timestamp=datetime.datetime.strptime(row['date'], "%Y-%m-%dT%H:%M:%S.%fZ") #row['date']
        self.start_time=self.timestamp #row['date']
        self.opn=row['open']
        self.cls=row['close']
        self.cmin=row['low']
        self.cmax=row['high']
        self.vol=row['volume']
        self.interval=interval
        self.closed=True

    def parseFromQuandl(self, timestamp, row, interval):
        """
        assume we can access the Candle data by row.opn, row.cls, row.min, row.max, row.vol
        """
        #print(row['date'])
        self.timestamp=timestamp
        self.start_time=self.timestamp #row['date']
        self.opn=row['Open']
        self.cls=row['Close']
        self.cmin=row['Low']
        self.cmax=row['High']
        self.vol=row['Volume']
        self.interval=interval
        self.closed=True

    def parseFromBitmexSocket(self, item, interval_mins):
        """Looks like this:
        {'timestamp': '2019-06-22T21:05:00.000Z',  the time when the CANDLE recording was COMPLETED!!!
        'symbol': 'XBTUSD', 'open': 10630, 
        'high': 10652, 'low': 10512, 'close': 10630, 
        'trades': 9467, 'volume': 47284988, 
        'vwap': 10576.4146, 'lastSize': 20000, 
        'turnover': 447116450476, 'homeNotional': 4471.164504760001, 
        'foreignNotional': 47284988}
        """
        dt=dateutil.parser.parse(item['timestamp'])
        #convert the timezone to berlin 
        #and remove the timezone stuff from the datetime object
        dt=dt.astimezone(local_timezone).replace(tzinfo=None)
        self.timestamp=dt

        self.start_time=self.timestamp-datetime.timedelta(minutes=interval_mins)
        self.opn=item['open']
        self.cls=item['close']
        self.cmin=item['low']
        self.cmax=item['high']
        self.vol=item['volume']
        self.interval=interval_mins
        #dt=datetime.datetime.now()-self.timestamp
        #logging.info("interval secs: "+str(interval_mins))
        self.closed=True #dt.seconds/60>interval_mins #dt.microseconds/1e-6

    def parseFromBinanceHTTP(self, arr, interval):
        #seef for docs:
        #http://python-binance.readthedocs.io/en/latest/binance.html#binance.client.Client.get_klines
        #timestamp, when the candle was CLSOED
        self.timestamp=datetime.datetime.fromtimestamp(arr[6]/1e3)

        self.start_time=datetime.datetime.fromtimestamp(arr[0]/1e3)-datetime.timedelta(minutes=interval)
        self.opn=float(arr[1])
        self.cls=float(arr[4])
        self.cmin=float(arr[3])
        self.cmax=float(arr[2])
        self.num_trades=float(arr[5])
        self.vol=float(arr[8]) #volume in this curreny. must be  the same vol as in parseFromBinance!
        self.interval=interval
        self.closed=True

    def parseFromMt(self, obj, interval):
        """
        receives an object in the format from the zmqtrader EA
        """
        self.timestamp=dateutil.parser.parse(obj["time"])
        self.start_time=dateutil.parser.parse(obj["time"])
        self.interval=interval
        self.opn=obj["open"]
        self.cls=obj["close"]
        self.cmin=obj["low"]
        self.cmax=obj["high"]
        self.vol=obj["volume"]
        self.closed=True


    def __str__(self):
        return "Candle "+self.symbol+":\n"+str(self.asDict())+"\n"


    def asList(self):
        out=[self.opn, self.cls, self.cmin, self.cmax, self.vol]
        return out

    def asNDArray(self):
        a=self.asList()
        return np.array(a)

    def isRising(self):
        return self.cls > self.opn

    def isFalling(self):
        return self.cls < self.opn

    def getSrc(self, src):
        """
        src: can be cls, hl2, hcl3 or what ever as defined in trading view
        """
        if src=="close":
            return self.cls
        if src=="hl2":
            return (self.cmax+self.cmin)/2.0
        if src=="hcl3":
            return (self.cmax+self.cmin+self.cls)/3.0

        if src=="open":
            return self.opn
        if src=="close":
            return self.cls
        if src=="max":
            return self.cmax
        if src=="min":
            return self.cmin


    def asDict(self):
        out={'open':self.opn, 'close':self.cls, 'min':self.cmin, 'max':self.cmax, 'vol':self.vol, 'timestamp':self.timestamp, 'interval':self.interval,
             'closed':self.closed, 'start_time':self.start_time}
        return out

if __name__ == "__main__":

    c=Candle("test")
    c.setMembers({'cls':3})