"""
Enables to simulate time in the past
"""


import time
import datetime

class TimeWall:
    def __init__(self, simulated=False):
        self.simulated=simulated
        self.reset()

    def reset(self):
        self.current_time=datetime.datetime.now()-datetime.timedelta(days=2400)


    def getCurrentTime(self):
        if self.simulated:
            return self.current_time
        else:
            return datetime.datetime.now()

    def setCurrentTime(self, curr):
        if self.current_time>curr:
            raise Exception("The current time is larger than the new time to be set. curr: "+str(self.current_time)+", new: "+str(curr))
        self.current_time=curr



timewall=TimeWall()

