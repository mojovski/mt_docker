
#property copyright "FH."
#property link      "https://www.funkyhedge.com/"
#property version   "2.0.1"
#property strict

// Required: MQL-ZMQ from https://github.com/dingmaotu/mql-zmq

#include <Zmq/Zmq.mqh>
#include <json.mqh>

extern string ZEROMQ_PROTOCOL = "tcp";
extern string HOSTNAME = "*";
//perspective from the client: 
//e.g. the client is pushing on the PUSH_PORT 32768
extern int PUSH_PORT = 32768;
extern int PULL_PORT = 32769;
extern int PUB_PORT = 32770;
extern int MILLISECOND_TIMER = 100;

extern string t0 = "--- Trading Parameters ---";
extern int MagicNumber = 123456;
extern int MaximumOrders = 1;
extern double MaximumLotSize = 0.01;
extern int MaximumSlippage = 3;
extern bool DMA_MODE = true;

extern string t1 = "--- ZeroMQ Configuration ---";
extern bool Publish_MarketData = true;



string published_symbols[1] = {
   "EURUSD"
};


// CREATE ZeroMQ Context
Context context();
// CREATE ZMQ_PUSH SOCKET
Socket pushSocket(context, ZMQ_PUSH);
// CREATE ZMQ_PULL SOCKET
Socket pullSocket(context, ZMQ_PULL);
// CREATE ZMQ_PUB SOCKET
Socket pubSocket(context, ZMQ_PUB);


/*
Initializes all required zmq ports
**/

void checkSocketRes(const int res)
{
  string s=IntegerToString(res);
  
   if (!res)
   {
     Print("Socket bind result (1: ok, 0: failed): "+s);
     int errno=zmq_errno();
     string err_str=zmq_strerror(errno);
     Print("Error on socket connection: "+IntegerToString(errno));
    }
 }
 

int OnInit()
  {
   EventSetMillisecondTimer(MILLISECOND_TIMER);     // Set Millisecond Timer to get client socket input
   context.setBlocky(false);
   /* Set Socket Options */
   // Send responses to PULL_PORT that client is listening on.
   pushSocket.setSendHighWaterMark(1);
   pushSocket.setLinger(0);
   Print("[PUSH] Binding MT4 Server to Socket on Port " + IntegerToString(PULL_PORT) + "..");
   bool res=pushSocket.bind(StringFormat("%s://%s:%d", ZEROMQ_PROTOCOL, HOSTNAME, PULL_PORT));
   checkSocketRes(res);
   
   // Receive commands from PUSH_PORT that client is sending to.
   pullSocket.setReceiveHighWaterMark(1);
   pullSocket.setLinger(0);
   Print("[PULL] Binding MT4 Server to Socket on Port " + IntegerToString(PUSH_PORT) + "..");   
   res=pullSocket.bind(StringFormat("%s://%s:%d", ZEROMQ_PROTOCOL, HOSTNAME, PUSH_PORT));
   checkSocketRes(res);
   
   if (Publish_MarketData == true)
   {
      // Send new market data to PUB_PORT that client is subscribed to.
      pubSocket.setSendHighWaterMark(100);
      pubSocket.setLinger(0);
      Print("[PUB] Binding MT4 Server to Socket on Port " + IntegerToString(PUB_PORT) + "..");
      res=pubSocket.bind(StringFormat("%s://%s:%d", ZEROMQ_PROTOCOL, HOSTNAME, PUB_PORT));
      checkSocketRes(res);
   }
   
   return(INIT_SUCCEEDED);
  }
  
  
/*
Destructor
*/
void OnDeinit(const int reason)
{
  //note, this is triggered also when the user changes the timeframe of the graph
  //so DO NOT CHANGE the time frame. otherwise, the sockets get blown up
   if (reason < 2)
    {
      Print("Destroying zMQ COntext...");
      context.destroy(0);
      EventKillTimer();
   }
}

/**
Triggered on every tick price change
**/
void OnTick()
{
   for(int s = 0; s < ArraySize(published_symbols); s++)
   {
        //Print("Getting "+published_symbols[s]);
        string tick_json_str = GetBidAsk(published_symbols[s]); 
        //Print("Sending " + published_symbols[s] + " " + tick_json_str + " to PUB Socket");

        sendDataToSocket(pubSocket, tick_json_str);
   }
  
     
}

/*
Is triggered on every timer event
*/
void OnTimer()
{
   if(CheckServerStatus() == true)
   {
      // Get client's response, but don't block.
      ZmqMsg request;
      pullSocket.recv(request, true);
      
      if (request.size() > 0)
         MessageHandler(request);
      //else
      //  Print("--");
   }
}


void MessageHandler(ZmqMsg & msg) {
   Print("Handling incoming message");
   if(msg.size() > 0) {
      uchar _data[];
   
      // Get data from request   
      ArrayResize(_data, msg.size());
      msg.getData(_data);
      string dataStr = CharArrayToString(_data);
      Print("handling message: "+dataStr);
      
      // Process data, see https://www.mql5.com/de/code/11134
      JSONParser parser();
      JSONValue *jv = parser.parse(dataStr);

      if (jv == NULL) {
        Print("Error on parsing the command: "+(string)parser.getErrorCode()+parser.getErrorMessage());
      } else {
        Print("Msg parsed!: "+jv.toString());

        JSONObject *jo = jv;
        string cmd_res="";
        //======
        //Now check all known possible values for cmd

        if (jo.getString("cmd") == "setSymbols")
             cmd_res = setSymbols(jo.getArray("symbols"));

        if (jo.getString("cmd") == "placeOrder")
            cmd_res = placeOrder(jo.getObject("order"));

        if (jo.getString("cmd") == "closePosition")
            cmd_res = closePosition(jo.getObject("position"));

        if (jo.getString("cmd") == "getOpenOrders")
          cmd_res = getOpenOrders();
          
         if (jo.getString("cmd") == "updateOrder")
          cmd_res = updateOrder(jo.getObject("position"));

        Print("Responding with "+cmd_res);
        sendDataToSocket(pushSocket, cmd_res);

      }
      //
      delete jv;
    }
}


string updateOrder(JSONObject & obj)
{
  int ticket=obj.getInt("order_id");
  float sl=obj.getDouble("sl");
  float tp=obj.getDouble("tp");
  //we need to select the order first...
  bool selection_ok=OrderSelect(ticket, SELECT_BY_TICKET);
  if (!selection_ok){
      return "{'state':'error', 'msg':'Could not find the order ID'}";
  }
  //OrderSelect(ticket,SELECT_BY_TICKET);

  bool update_res = OrderModify(OrderTicket(),OrderOpenPrice(), sl, tp, 0, Blue);
  if (update_res==true)
  {
      return "{'state':'ok', 'msg':'Order modified'}";
  }
  else {
      int msg=IntegerToString(GetLastError());
      return "{'state':'error', 'msg':'Could not modify the order: "+msg+"'}";
  }


}

/*
Closes a position my market
*/
string closePosition(JSONObject & obj)
{
    int ticket=obj.getInt("order_id");
    //we need to select the order first...
    bool selection_ok=OrderSelect(ticket, SELECT_BY_TICKET);
    if (!selection_ok){
        return "{'state':'error', 'msg':'Could not find the order ID'}";
    }

    double lots=OrderLots(); 
    int order_type=OrderType();
    string symbol=OrderSymbol();
    double price=getCurrentExitPrice(symbol, order_type);
    double slippage=obj.getDouble("slippage");

    bool close_res=OrderClose(ticket, lots, price, slippage);
    if (close_res==true)
    {
        return "{'state':'ok', 'msg':'Position closed'}";
    }
    else {
        int msg=IntegerToString(GetLastError());
        return "{'state':'error', 'msg':'Could not close the position: "+msg+"'}";
    }

}

string placeOrder(JSONObject & obj)
{

    int order_id = OrderSend(obj.getString("symbol"),
        obj.getInt("type"), obj.getDouble("lots"), getCurrentEntryPrice(obj),  
        obj.getDouble("max_slippage"), obj.getDouble("SL"), 
        obj.getDouble("TP"),
        obj.getString("comment"),
        obj.getInt("magic"));

    string res;

    if(order_id < 0) {
        // Failure
        int error = GetLastError();
        string msg  = "Response: " + IntegerToString(error);

        res="{'state':'error', 'msg':'"+msg+"', 'order_id':0}";
   } else
   {
        res="{'state':'ok', 'msg':'-', 'order_id':"+IntegerToString(order_id)+"}";
   }
   
   return res;
}

//returns the current price (bid or ask) based on the order type (buy or sell)
double getCurrentEntryPrice(JSONObject & obj)
{
    if (obj.getInt("type")==OP_BUY)
    {
        double price=MarketInfo(obj.getString("symbol"), MODE_ASK);
        return price;
    } 
    if (obj.getInt("type")==OP_SELL)
    {
        double price=MarketInfo(obj.getString("symbol"), MODE_BID);
        return price;
    }

    Print("Error: the order type is not supported");
    return 0;

}

//returns the current price (bid or ask) based on the order type (buy or sell) to close/exit
double getCurrentExitPrice(string symbol, int order_type)
{
    if (order_type==OP_BUY)
    {
        double price=MarketInfo(symbol, MODE_BID);
        return price;
    } 
    if (order_type==OP_SELL)
    {
        double price=MarketInfo(symbol, MODE_ASK);
        return price;
    }

    Print("Error: the order type is not supported");
    return 0;

}


/**
Takes a json array and write its elements to the
*/
string setSymbols(JSONArray & list)
{
  Print("Setting symbols..."+list.toString());
  ArrayResize(published_symbols, list.size());
  for(int i=0; i<list.size(); i++)
  {
    published_symbols[i]=list.getString(i);
  }
  return "{'state': 'ok'}";
}

/*
returns a list of all open positions
**/
string getOpenOrders()
{
  string res="[";
  //OPEN ORDERS EXIST
  Print("Found "+IntegerToString(OrdersTotal())+" total orders");
   if(OrdersTotal() > 0)
   {
      //INITIALIZE TICKET VARIABLES
      int ticket_number, ticket_order_type, ticket_currency_digits, ticket_currency_multiplier;
      string ticket_order_type_name;
      double ticket_profit=0.0;
      
      //LOOK THROUGH ALL OPEN ORDERS
      for(int order_counter = 0; order_counter < OrdersTotal(); order_counter ++)
      {
         //CURRENT OPEN ORDER MATCHES CURRENT SYMBOL
         if(OrderSelect(order_counter, SELECT_BY_POS, MODE_TRADES) == true)// && OrderSymbol() == Symbol())
         {
            //RETRIEVE OPEN ORDER'S TICKET NUMBER
            ticket_number = OrderTicket();
            
            //RETRIEVE OPEN ORDER'S TYPE
            ticket_order_type = OrderType();
            
            //RETRIEVE OPEN ORDER'S CURRENCY DIGITS
            ticket_currency_digits = MarketInfo(OrderSymbol(), MODE_DIGITS);
            
            //RETRIEVE OPEN ORDER'S CURRENCY MULTIPLIER TO CALCULATE NUMBER OF PIPS
            ticket_currency_multiplier=1;
            switch(ticket_currency_digits)
            {
               //CURRENCIES WITH 2 DIGITS
              
               case 2:
                  ticket_currency_multiplier = 100;
                  break;
               
               //CURRENCIES WITH 4 DIGITS   
               case 4:
                  ticket_currency_multiplier = 10000;
                  break;
            }
            
            //RETRIEVE OPEN ORDER'S TYPES NAME & PROFIT IN PIPS          
            switch(ticket_order_type)
            {
               //BUYING POSITION
               case 0:
                  ticket_order_type_name = "BUY";
                  ticket_profit = (OrderClosePrice() - OrderOpenPrice()) * ticket_currency_multiplier;
                  break;
               
               //SELLING POSITION
               case 1:
                  ticket_order_type_name = "SELL";
                  ticket_profit = (OrderOpenPrice() - OrderClosePrice()) * ticket_currency_multiplier;
                  break;
            }

            string item="{'symbol':'"+OrderSymbol()+"', "+
            "'qty':"+DoubleToString(OrderLots())+", "+
            "'type': '"+ticket_order_type_name+"', 'id':"+IntegerToString(ticket_number)+", 'open_price':"+
            DoubleToString(OrderOpenPrice())+", 'close_price':"+DoubleToString(OrderClosePrice())+", 'profit_pips':"+
            DoubleToString(ticket_profit)+", 'SL':"+DoubleToString(OrderStopLoss())+", 'TP':"+DoubleToString(OrderTakeProfit())+"}";
            
            

            /*Alert(OrderSymbol() + " " + ticket_order_type_name + " Ticket: " + ticket_number + ", Open Price: " +
            OrderOpenPrice() + ", Close Price: " + OrderClosePrice() + ", P/L: " + ticket_profit + " Pips , Stop Loss: " +
            OrderStopLoss() + ", Take Profit: " + OrderTakeProfit());
            */
            if (order_counter>0)
            {
            res=res+", ";
            }
            res=res+item;
            
        }
        
      }
   }
   res=res+"]";
   return res;
}


/*
Verifies that the EA is still running
*/
bool CheckServerStatus() {

   // Is _StopFlag == True, inform the client application
   if (IsStopped()) {
      sendDataToSocket(pullSocket, "{'_response': 'EA_IS_STOPPED'}");
      return(false);
   }
   
   // Default
   return(true);
}




//+------------------------------------------------------------------+
// Generate string for Bid/Ask by symbol
string GetBidAsk(string symbol) {
   MqlTick last_tick;
   if(SymbolInfoTick(symbol,last_tick))
   {
        return(StringFormat("{\"bid\":%f, \"ask\":%f, \"symbol\":\"%s\"}", last_tick.bid, last_tick.ask, symbol));
   }
   return "";
}


// Inform Client
void sendDataToSocket(Socket& pSocket, string message) {
  ZmqMsg pushReply(message); //StringFormat("%s", message));
  bool res= pSocket.send(pushReply,true); // NON-BLOCKING
  if(!res){
   Print("Sending socket data returned false");
  }
}
