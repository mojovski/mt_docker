#property copyright "Copyright 2022, Safety-Net.Capital"
#property link      "https://safety-net.capital"


class HDTime
{
    private:
    datetime init_time; //sec-resolution time when the object is created
    ulong offset; //Offset in ms since system launch
  public:
    HDTime(void);
    ulong getAbsTimeInMs(); //Returns the absolute time in ms since 1970
};

HDTime::HDTime(void)
{
  /*
  initializes the high resolution timer
  */
  init_time = TimeCurrent();
  offset = GetTickCount64();
}

ulong HDTime::getAbsTimeInMs()
{
    datetime now = TimeCurrent();
    ulong tc_ms = GetTickCount64();
    ulong now_ms = ulong(init_time)*1000 + tc_ms - offset;
    return now_ms;
}
