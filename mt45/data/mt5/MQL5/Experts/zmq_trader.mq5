
#property copyright "Safety Net by ProjektF GmbH."
#property link      "https://www.safety-net.capital/"
#property version   "2.0"
#property strict

// Required: MQL-ZMQ from https://github.com/dingmaotu/mql-zmq

#include <Zmq/Zmq.mqh>
#include <JAson.mqh>
#include <Generic/HDTime.mqh>

extern string ZEROMQ_PROTOCOL = "tcp"; 
extern string HOSTNAME = "*";
//perspective from the client: 

// Trigger on timer or Tick
enum TriggerOption{
   T_ontimer = 1, //Publish on Timer
   T_ontick = 2, //Publish on tick
};

input TriggerOption TRIGGER_OPTION = T_ontick;


//e.g. the client is pushing on the REP_PORT 32768
//now everything works on one port instead of two
input int REP_PORT = 32768; //Request/Response port


input int PUB_PORT = 32770; //Publishing port
input int MILLISECOND_TIMER = 100; //Timer in ms
input bool USE_TOPICS= true; //Use Topics for pub/sub




extern string t1 = "--- ZeroMQ Configuration ---";
extern bool Publish_MarketData = true;

#include <Trade\Trade.mqh>
CTrade ctrade;
COrderInfo orderinfo;
CPositionInfo posInfo;

string published_symbols[];
MqlTick prev_ticks[];

HDTime hd_time;
string broker_name;

// CREATE ZeroMQ Context
Context context();
// CREATE ZMQ_REP SOCKET
Socket repSocket(context, ZMQ_REP);

// CREATE ZMQ_PUB SOCKET
Socket pubSocket(context, ZMQ_PUB);



/** on ini of the EA. also called when the time frame changes
**/
int OnInit()
  {
    Print("Executing OnInit");

    ArrayResize(published_symbols, 1);
    published_symbols[0] = Symbol();
    initSockets();
    Print("INitialized with symbol: "+Symbol());
    broker_name = AccountInfoString(ACCOUNT_COMPANY);
    StringReplace(broker_name, " ", "_");

    
    EventSetMillisecondTimer(MILLISECOND_TIMER);     // Set Millisecond Timer to get client socket input
    
   return(INIT_SUCCEEDED);
  }
  
  
/*
Destructor
*/
void OnDeinit(const int reason)
{
   Print("Deinit called. Reason: "+IntegerToString(reason));
   //Print("Reson: ",getUninitReasonText(UninitializeReason()));
   //if (reason<2) //deinit only if something severe happened.
    //see https://www.mql5.com/de/docs/constants/namedconstants/uninit
    //{
      closeSockets();
    //}
}


void checkSocketRes(const bool res)
{
  string s=IntegerToString(res);
  
   if (!res)
   {
      Print("Socket bind res(1: true, 0:false): "+s);
      int errno=zmq_errno();
      string err_str=zmq_strerror(errno);
      Print("Error on socket connection: "+IntegerToString(errno)+". Msg: "+err_str);
    }
 }


void initSockets()
{
  context.setBlocky(false);
  /* Set Socket Options */
  // Send responses to REP_PORT that client is listening on.
  Print("[REP] Binding MT Server to Socket on Port " + IntegerToString(REP_PORT) + "..");
  repSocket.setSendHighWaterMark(1);
  repSocket.setLinger(0);
  bool res=repSocket.bind(StringFormat("%s://%s:%d", ZEROMQ_PROTOCOL, HOSTNAME, REP_PORT));
  checkSocketRes(res);

   
  if (Publish_MarketData == true)
   {
      // Send new market data to PUB_PORT that client is subscribed to.
      Print("[PUB] Binding MT Server to Socket on Port " + IntegerToString(PUB_PORT) + "..");
      pubSocket.setSendHighWaterMark(100);
      pubSocket.setLinger(0);
      res=pubSocket.bind(StringFormat("%s://%s:%d", ZEROMQ_PROTOCOL, HOSTNAME, PUB_PORT));
      checkSocketRes(res);
   }

}


void closeSockets()
{
  if (Publish_MarketData == true)
   {
      Print("[pubSocket] Unbinding MT Server from Socket on Port " + IntegerToString(PUB_PORT) + "..");
      pubSocket.unbind(StringFormat("%s://%s:%d", ZEROMQ_PROTOCOL, HOSTNAME, PUB_PORT));
      pubSocket.disconnect(StringFormat("%s://%s:%d", ZEROMQ_PROTOCOL, HOSTNAME, PUB_PORT));
    }


  Print("[repSocket] Unbinding MT Server from Socket on Port " + IntegerToString(REP_PORT) + "..");
  repSocket.unbind(StringFormat("%s://%s:%d", ZEROMQ_PROTOCOL, HOSTNAME, REP_PORT));
  repSocket.disconnect(StringFormat("%s://%s:%d", ZEROMQ_PROTOCOL, HOSTNAME, REP_PORT));

  // Destroy ZeroMQ Context
  context.destroy(0);
  EventKillTimer();

}


/**
Triggered on every tick price change
**/
void OnTick()
{
  if (TRIGGER_OPTION==T_ontick)
  {
    pollTicks();
  }
  return; 
}

void pollTicks()
{
  /**
  Updates and publishes the newest tick data from all subscribed symbols
  **/
  for(int s = 0; s < ArraySize(prev_ticks); s++)
    {
      MqlTick last_tick;
      if(SymbolInfoTick(published_symbols[s],last_tick))
        {
            if (prev_ticks[s].ask == last_tick.ask && prev_ticks[s].bid ==last_tick.bid)
              continue;
            prev_ticks[s]=last_tick;
            string topic = "";
            if (USE_TOPICS)
            {
              topic = "tick."+broker_name+"."+published_symbols[s];
            }
            
            string tick_json_str=tick2json(last_tick, published_symbols[s], "");
            
            sendDataToSocket(pubSocket, tick_json_str, topic);

       } else {
          PrintFormat("%s: Error on polling tick data for %s: %d", __FUNCTION__, published_symbols[s], GetLastError());
       }

   }

}


/*
Is triggered on every timer event
*/
void OnTimer()
{
   if(CheckServerStatus() == true)
   {
      // Get client's response, but don't block.
      ZmqMsg request;
      repSocket.recv(request, true);
      
      if (request.size() > 0)
         MessageHandler(request);

      if (TRIGGER_OPTION==T_ontimer)
        pollTicks();
   }
}


void MessageHandler(ZmqMsg & msg) {
  /*
  Processes the message received over zmq request
  */
   if(msg.size() > 0) {
      uchar _data[];
   
      // Get data from request   
      ArrayResize(_data, msg.size());
      msg.getData(_data);
      string dataStr = CharArrayToString(_data);
      Print("handling message: "+dataStr);
      
      // Process data, see https://www.mql5.com/de/code/11134
      CJAVal jv(NULL, jtUNDEF); bool b;
      b=jv.Deserialize(dataStr);
      
      //JSONParser parser();
      //JSONValue *jv = parser.parse(dataStr);

      if (b==false) {
        Print("Error on parsing the msg: "+dataStr);
      } else {
      //  Print("Msg parsed!: "+jv.ToStr());


        string cmd_res="";
        //======
        //Now check all known possible values for cmd

        if (jv["cmd"] == "setSymbols")
             cmd_res = setSymbols(jv["symbols"]);

        if (jv["cmd"] == "addSymbol")
        {
          cmd_res = addSymbol(jv["symbol"].ToStr());
        }

        if (jv["cmd"] == "placeOrder")
            cmd_res = placeOrder(jv["order"]);

        if (jv["cmd"] == "closePosition")
            cmd_res = closePosition(jv["position"]);

        if (jv["cmd"] == "getOpenOrders")
          cmd_res = getOpenOrders();

        if (jv["cmd"] == "getAccountInfo")
        {
          cmd_res = getAccountInfo();
          Print(cmd_res);
        }

        if (jv["cmd"]=="getPastCandles")
        {
          Print("getting past candles");
          cmd_res = getPastCandles(jv["symbol"].ToStr(), strToEnumTimeframe(jv["timeframe"].ToStr()), jv["num"].ToInt());
        }
        if (jv["cmd"]=="getPastCandlesRange")
        {
          Print("getting past candles");
          datetime t0=StringToTime(jv["start_time"].ToStr()); //"yyyy.mm.dd [hh:mi:ss]" more: https://www.mql5.com/de/docs/convert/stringtotime
          datetime t1=StringToTime(jv["end_time"].ToStr());
          cmd_res = getPastCandlesRange(jv["symbol"].ToStr(), strToEnumTimeframe(jv["timeframe"].ToStr()), t0, t1);
        }
        if (jv["cmd"]=="getPastTicks")
        {
          Print("getting past ticks");
          cmd_res = getPastTicks(jv["symbol"].ToStr(), (ulong)(jv["start_time"].ToInt()), jv["num"].ToInt());
        }
          
         if (jv["cmd"] == "updateOrder")
          cmd_res = updateOrder(jv["position"]);

        
        sendDataToSocket(repSocket, cmd_res);
        Print("done");

      }
    }
}

string getAccountInfo()
{
  /*
  Returns the current equity and some other information
  */
  double eqty=AccountInfoDouble(ACCOUNT_EQUITY);
  double balance=AccountInfoDouble(ACCOUNT_BALANCE);
  double profit=AccountInfoDouble(ACCOUNT_PROFIT);
  double margin=AccountInfoDouble(ACCOUNT_MARGIN);
  double free_margin=AccountInfoDouble(ACCOUNT_MARGIN_FREE);
  string open_positions=getOpenOrders();
  return "{'state':'ok', 'equity':"+DoubleToString(eqty)+", 'profit':"+DoubleToString(profit)+
    ", 'margin':"+DoubleToString(margin)+", 'free_margin':"+DoubleToString(free_margin)+
    ", 'balance':"+DoubleToString(balance)+", 'positions':"+open_positions+"}";

}

string getPastTicks(string symbol, ulong unix_start_time, int num_to_load)
{
  /*
  Returns historical tick data.
  unix_start_time: Milliseconds from 1.1.1970. If 0, num_to_load is used
  num_to_load: amount of ticks to get
  **/
  MqlTick tick_array[];
  int num_copied = CopyTicks(symbol, tick_array, COPY_TICKS_ALL, unix_start_time, num_to_load);
  string res="[";
  if (num_copied<0)
  {
    string err_code=IntegerToString(GetLastError());
    return "{'state':'error', 'msg':'Could not get past ticks. Error Code: "+err_code+"'}";

  } else
  {
    //int ms=(int)(tick_array[num_copied-1].time-(int)(tick_array[num_copied-1].time/1e3)*1e3);
    //Print("original time stamp: "+DoubleToString(tick_array[num_copied-1].time)+", ms: "+IntegerToString(ms));
    //Print("First tick is from: "+TimeToString(tick_array[num_copied-1].time)+","+IntegerToString(ms));
    string format="'datatype':'tick', 'bid':%G, 'ask':%G, 'last':%G, 'type':%i, 'volume':%d";
    for(int i=0;i<num_copied;i++)
    {
      string ci="{'time':'"+(string)(tick_array[i].time_msc)+"', "+ //DoubleToString(tick_array[i].time)+"', "+
        StringFormat(format,
                    tick_array[i].bid,
                    tick_array[i].ask,
                    tick_array[i].last,
                    tick_array[i].flags,
                    tick_array[i].volume_real)+"}";
        res=res+ci;
        if (i < (num_copied-1))
        {
          res=res+",";
        }
    }
  }
  res=res+"]";

  return res;
}

ENUM_TIMEFRAMES strToEnumTimeframe(string tf)
{
  //timeframes: https://www.mql5.com/de/docs/constants/chartconstants/enum_timeframes
  /*
  0: currently selected in the ui
  1: 1min
  2: 2min
  3: ...
  7: 10 minutes
  ...
  */

  if (tf=="1m")
    return PERIOD_M1;

  if (tf=="2m")
    return PERIOD_M2;

  if (tf=="3m")
    return PERIOD_M3;

  if (tf=="4m")
    return PERIOD_M4;

  if (tf=="5m")
    return PERIOD_M5;

  if (tf=="10m")
    return PERIOD_M10;

  if (tf=="15m")
    return PERIOD_M15;

  if (tf=="30m")
    return PERIOD_M30;

  if (tf=="1h")
    return PERIOD_H1;

  if (tf=="4h")
    return PERIOD_H4;

  if (tf=="1d")
    return PERIOD_D1;

  Print("Could not decode passed timeframe: "+tf);
  return PERIOD_CURRENT; 

}


string getPastCandlesRange(string symbol, ENUM_TIMEFRAMES tf, datetime start_time, datetime end_time)
{
  /*
  returns past candles. the first element is the newest
  */
  MqlRates rates[];
  ArraySetAsSeries(rates,true);

  //see https://www.mql5.com/en/docs/series/copyrates
  int num_copied=CopyRates(symbol,tf,start_time, end_time, rates);
  string res="[";
  if(num_copied>0)
  {
    Print("OHCL rate bars copied: "+IntegerToString(num_copied));
    string format="'open':%G, 'high':%G, 'low':%G, 'close':%G, 'volume':%d";
    for(int i=0;i<num_copied;i++)
    {
      string ci="{'time':'"+TimeToString(rates[i].time)+"', "+
        StringFormat(format,
                    rates[i].open,
                    rates[i].high,
                    rates[i].low,
                    rates[i].close,
                    rates[i].tick_volume)+"}";
        res=res+ci;
        if (i < (num_copied-1))
        {
          res=res+",";
        }
    }

  } else 
  {
    string err_code=IntegerToString(GetLastError());
    return "{'state':'error', 'msg':'Could not get past candles. Error Code: "+err_code+"'}";

  }

  res=res+"]";

  return res;
}


string getPastCandles(string symbol, ENUM_TIMEFRAMES tf, int num)
{
  /*
  returns past candles. the first element is the newest
  */
  MqlRates rates[];
  ArraySetAsSeries(rates,true);

  //see https://www.mql5.com/en/docs/series/copyrates
  int num_copied=CopyRates(symbol,tf,0,num,rates);
  string res="[";
  if(num_copied>0)
  {
    Print("OHCL rate bars copied: "+IntegerToString(num_copied));
    string format="'open':%G, 'high':%G, 'low':%G, 'close':%G, 'volume':%d";
    for(int i=0;i<num_copied;i++)
    {
      string ci="{'time':'"+TimeToString(rates[i].time)+"', "+
        StringFormat(format,
                    rates[i].open,
                    rates[i].high,
                    rates[i].low,
                    rates[i].close,
                    rates[i].tick_volume)+"}";
        res=res+ci;
        if (i < (num_copied-1))
        {
          res=res+",";
        }
    }

  } else 
  {
    string err_code=IntegerToString(GetLastError());
    return "{'state':'error', 'msg':'Could not get past candles. Error Code: "+err_code+"'}";

  }

  res=res+"]";

  return res;
}


string updateOrder(CJAVal & obj)
{
  /*
  Updates an existing order using the key order_id as the order ticket number
  */
  int ticket=obj["order_id"].ToInt();
  float sl=obj["sl"].ToDbl();
  float tp=obj["tp"].ToDbl();
  //we need to select the order first...
  bool selection_ok = PositionSelectByTicket(ticket);
  if (!selection_ok){
      return "{'state':'error', 'msg':'Could not find the order ID "+IntegerToString(ticket)+"'}";
  }

  bool pos_change_ok=ctrade.PositionModify(ticket, sl, tp);

  if(!pos_change_ok)
  {
    PrintFormat("OrderSend error %d",GetLastError());  // if unable to send the request, output the error code
    
    int msg=IntegerToString(GetLastError());
    return "{'state':'error', 'msg':'Could not modify the order: "+msg+"'}";
  } else {
    return "{'state':'ok', 'msg':'Order modified'}";
  }
   

}


string closePosition(CJAVal & obj)
{
    /*
    Closes a position my market. Supports also partial closing.
    */
    ulong order_id=(ulong)obj["order_id"].ToInt();
    double qty=obj["qty"].ToDbl();
    bool close_res = false;

    Print("Trying to close the position with ID: "+IntegerToString(order_id));

    //get the order state in case it is still pending
    bool pos_selected = PositionSelectByTicket(order_id); //posInfo.Select(order_id); 
    bool pending_order_selected = OrderSelect(order_id); //

    Print("pos_selected: "+IntegerToString(pos_selected)+", true is "+IntegerToString(true));
    Print("pending_order_selected: "+IntegerToString(pending_order_selected)+", true is "+IntegerToString(true));

    if (!pos_selected && !pending_order_selected){
      return "{'state':'error', 'msg':'Order or Position not found'}";
    }

    
    ENUM_ORDER_STATE state = ORDER_STATE_STARTED; //ORDER_STATE_
    if (pending_order_selected)
    {
      state= orderinfo.State();
      Print("State of the selected order: "+EnumToString(state));
    }

    //Handling ORDER
    if (pending_order_selected && state!=ORDER_STATE_FILLED)
    {
      //this is a pending order
      Print("The order has not been filled yet. State: "+IntegerToString(state)+". Cancelling");
      close_res = ctrade.OrderDelete(order_id);
      return "{'state':'ok', 'msg':'Order deleted', 'exit_price': 0, 'close_state':'"+IntegerToString(state)+"'}";
    }
    
    //Handling Position
    if (qty==0)
    {
      close_res = ctrade.PositionClose(order_id, 10);
    } else {
      string comment = PositionGetString(POSITION_COMMENT);
      close_res = myPartialClose(order_id, qty, comment);
    }
    

    if (close_res==true)
    {
        //get the exit price of the position
        //bool order_ok=OrderSelect(order_id);
        double exit_price= ctrade.RequestPrice(); // OrderGetDouble(ORDER_PRICE_CURRENT);

        return "{'state':'ok', 'msg':'Position closed', 'exit_price':"+DoubleToString(exit_price)+"}";
    }
    else {
        int msg=IntegerToString(GetLastError());
        int err_code=ctrade.ResultRetcode();
        return "{'state':'error', 'msg':'Could not close the position. Err code: "+IntegerToString(err_code)+", error: "+ctrade.ResultRetcodeDescription()+"'}";
    }

}

bool myPartialClose(int order_id, double qty, string comment)
{
  /* we use this since the ctrade.partialClose does not support keeping the comments **/

  bool close_res = ctrade.PositionClosePartial(order_id, qty, 10);
  return close_res;
}

string getCommentFromHistoryPartialClose(int order_id, int num_back)
{
  /*
  if a position is partially closed, MT deletes the comment.
  This method tries to reconstruct the comment from the history of orders
  matching the passed order_id.
  If a comment of the original position (nor partially closed) is found
  its comment is returned here.
  */

  string comment="";

  bool pos_selected = PositionSelectByTicket(order_id);
  if (!pos_selected)
  {
    Print("Could not find the position from id: "+IntegerToString(order_id));
    return "";
  }

  comment = PositionGetString(POSITION_COMMENT);
  //Print("Comment: x"+comment+"x");
  
  if (StringCompare(comment, "")!=0)
  {
    //Print("Direct comment ok: "+comment);
    return comment;    
  }
  //Print("Searching in history for a good comment");


  //if the comment is empty, try to get the comment form the past positions (in case of a partial close)

  HistorySelect(0,TimeCurrent());
  int nb = int (MathMin(num_back, HistoryDealsTotal()));
  int ntotal = HistoryDealsTotal();
  //Print("ntotal: "+IntegerToString(ntotal)+", nb: "+IntegerToString(nb));

  for (int i=0; i<nb; i++)
  {
    //Print("i: "+IntegerToString(i));

    ulong ticket_id = HistoryDealGetTicket(ntotal-1-i);
    if (ticket_id==0)
    {
      Print("Ticket "+IntegerToString(ticket_id)+" not found");
      continue;
    }
    comment = HistoryDealGetString(ticket_id, DEAL_COMMENT);
    int pos_order_id = HistoryDealGetInteger(ticket_id, DEAL_POSITION_ID);
    double vol = HistoryDealGetDouble(ticket_id, DEAL_VOLUME);

    //Print("Deal id: "+IntegerToString(pos_order_id)+", query id: "+IntegerToString(order_id)+", vol: "+DoubleToString(vol));
    if (order_id==pos_order_id)
    {
      comment = HistoryDealGetString(ticket_id, DEAL_COMMENT);
      //Print("Found matching (partial?) comment: "+comment);
      if (StringCompare(comment, "")!=0)
      { 
        //Print("History comment ok: "+comment);
        return comment;    
      }
    }
  }
  
  //finally, return the probably empty comment string
  return "";

  
}


/*
places an order
**/
string placeOrder(CJAVal & obj)
{
  Print("symbol: "+obj["symbol"].ToStr());
  Print("type: "+EnumToString((ENUM_ORDER_TYPE)obj["type"].ToInt()));
  Print("Vol: "+DoubleToString(obj["lots"].ToDbl()));
  Print("SL: "+DoubleToString(obj["SL"].ToDbl()));
  Print("TP: "+DoubleToString(obj["TP"].ToDbl()));

  ENUM_ORDER_TYPE order_type= (ENUM_ORDER_TYPE)obj["type"].ToInt();
  bool res_process;
  datetime expiration_time = 0; //TimeCurrent()+9923*60*60;
  Print("Expiratio time: "+TimeToString(expiration_time));
  if (order_type==ORDER_TYPE_BUY)
  { //https://www.mql5.com/en/docs/standardlibrary/tradeclasses/ctrade/ctradebuy
    res_process= ctrade.Buy(obj["lots"].ToDbl(), obj["symbol"].ToStr(), 0.0, obj["SL"].ToDbl(), obj["TP"].ToDbl(),  obj["comment"].ToStr());
  } else if (order_type == ORDER_TYPE_SELL)
  {
    res_process= ctrade.Sell(obj["lots"].ToDbl(), obj["symbol"].ToStr(), 0.0, obj["SL"].ToDbl(), obj["TP"].ToDbl(),  obj["comment"].ToStr());
  } else if (order_type == ORDER_TYPE_BUY_LIMIT)
  {
    res_process= ctrade.BuyLimit(obj["lots"].ToDbl(), obj["price"].ToDbl(), obj["symbol"].ToStr(),  obj["SL"].ToDbl(), obj["TP"].ToDbl(), ORDER_TIME_GTC, expiration_time,   obj["comment"].ToStr());
  } else if (order_type == ORDER_TYPE_SELL_LIMIT)
  {
    res_process= ctrade.SellLimit(obj["lots"].ToDbl(), obj["price"].ToDbl(), obj["symbol"].ToStr(),  obj["SL"].ToDbl(), obj["TP"].ToDbl(),   ORDER_TIME_GTC, expiration_time, obj["comment"].ToStr());
  } else if (order_type == ORDER_TYPE_BUY_STOP)
  {
    res_process= ctrade.BuyStop(obj["lots"].ToDbl(), obj["price"].ToDbl(), obj["symbol"].ToStr(),  obj["SL"].ToDbl(), obj["TP"].ToDbl(),  ORDER_TIME_GTC, expiration_time, obj["comment"].ToStr());
  } else if (order_type == ORDER_TYPE_SELL_STOP)
  {
    res_process= ctrade.SellStop(obj["lots"].ToDbl(), obj["price"].ToDbl(), obj["symbol"].ToStr(),  obj["SL"].ToDbl(), obj["TP"].ToDbl(),   ORDER_TIME_GTC, expiration_time, obj["comment"].ToStr());
  }
  
  if (!res_process) //if placing failed 
  {      
    Print("Failed submitting the order");
    int res_code=ctrade.ResultRetcode();
    string out="{'state':'error', 'msg':'Failed to process the order. MT5 err code:"+res_code+", "+ctrade.ResultRetcodeDescription()+" '}";
    return out;
  }

    int order_id=ctrade.ResultOrder();
    double open_price = ctrade.RequestPrice();
  string res="";
  if(order_id<1) 
  {
      // Failure
      int error = GetLastError();
      string msg  = "Response: " + IntegerToString(error);

      res="{'state':'error', 'msg':'"+msg+"', 'order_id':0}";
   } else
   {
        res="{'state':'ok', 'msg':'-', 'order_id':"+IntegerToString(order_id)+", 'open_price': "+DoubleToString(open_price)+"}";
   }
   
   return res;
}

//returns the current price (bid or ask) based on the order type (buy or sell)
double getCurrentEntryPrice(CJAVal & obj)
{
    if ((ENUM_ORDER_TYPE)obj["type"].ToInt()==ORDER_TYPE_BUY)
    {
        double price= SymbolInfoDouble(obj["symbol"].ToStr(),SYMBOL_ASK); //MarketInfo(obj.getString("symbol"), MODE_ASK);
        return price;
    } 
    if ((ENUM_ORDER_TYPE)obj["type"].ToInt()==ORDER_TYPE_SELL)
    {
        double price=SymbolInfoDouble(obj["symbol"].ToStr(),SYMBOL_BID); //MarketInfo(obj.getString("symbol"), MODE_BID);
        return price;
    }

    Print("Error: the order type is not supported");
    return 0;

}

//returns the current price (bid or ask) based on the order type (buy or sell) to close/exit
double getCurrentExitPrice(string symbol, int order_type)
{
    if (order_type==ORDER_TYPE_BUY)
    {
        double price=SymbolInfoDouble(symbol,SYMBOL_BID);
        return price;
    } 
    if (order_type==ORDER_TYPE_SELL)
    {
        double price=SymbolInfoDouble(symbol,SYMBOL_ASK);
        return price;
    }

    Print("Error: the order type is not supported");
    return 0;

}


/**
Takes a json array and write its elements to the
*/
string setSymbols(CJAVal & list)
{
  Print("Setting symbols..."+list.ToStr()+", of size: "+IntegerToString(list.Size()));
  Print("Prev array:");
  ArrayPrint(published_symbols);

  ArrayResize(published_symbols, list.Size());
  ArrayResize(prev_ticks, list.Size());
  
  ArrayPrint(published_symbols);

  for(int i=0; i<list.Size(); i++)
  {
    string item=list[i].ToStr();
    Print("item: "+item);
    published_symbols[i]=item;
    Print("set "+item);
    MqlTick last_tick;
    SymbolInfoTick(item,last_tick);
    prev_ticks[i]=last_tick;
  }
  ArrayPrint(published_symbols);
  return "{'state': 'ok'}";
}

string addSymbol(string s)
{
  Print("Prev array:");
  ArrayPrint(published_symbols);

  //check if the saymbol is already under subscription
  bool found = false;
  for (int i=0; i< ArraySize(published_symbols); i++)
  {
    if (published_symbols[i]==s)
      found=true;
  }
  if (found==true)
  {
    return "{'state': 'ok', 'msg':'Symbol already under subscription'}";
  }

  ArrayResize(published_symbols, ArraySize(published_symbols)+1);
  published_symbols[ArraySize(published_symbols)-1]=s;

  ArrayResize(prev_ticks, ArraySize(published_symbols)); //wel have to synchronize the stored ticks array p[i]=symbol[i]
  
  Print("New published symbols:");
  ArrayPrint(published_symbols);

  //Update the ticks
  for(int i=0; i<ArraySize(published_symbols); i++)
  {
    string symbol=published_symbols[i];
    Print("symbol: "+symbol);
    MqlTick last_tick;
    SymbolInfoTick(symbol,last_tick);
    prev_ticks[i]=last_tick;
  }
  //ArrayPrint(published_symbols);
  return "{'state': 'ok'}";
}


/*
returns a list of all open positions
**/
string getOpenOrders()
{
  string res="[";
  //OPEN ORDERS EXIST
  Print("Found "+IntegerToString(PositionsTotal())+" total positions");
   if(PositionsTotal() > 0)
   {
      //INITIALIZE TICKET VARIABLES
      int ticket_number, ticket_order_type, ticket_currency_digits, ticket_currency_multiplier;
      string ticket_order_type_name;
      double position_profit=0.0;
      
      //LOOK THROUGH ALL OPEN ORDERS
      for(int pos_counter = 0; pos_counter < PositionsTotal(); pos_counter ++)
      {
         //CURRENT OPEN ORDER MATCHES CURRENT SYMBOL
         if(PositionGetTicket(pos_counter) >0)// && OrderSymbol() == Symbol())
         {
            ticket_number = PositionGetTicket(pos_counter);
            PositionSelectByTicket(ticket_number);
            string symbol=PositionGetSymbol(pos_counter);
            double vol=PositionGetDouble(POSITION_VOLUME);
            position_profit=PositionGetDouble(POSITION_PROFIT);
            double open_price=PositionGetDouble(POSITION_PRICE_OPEN);
            double sl=PositionGetDouble(POSITION_SL);
            double tp=PositionGetDouble(POSITION_TP);
            double curr_price=PositionGetDouble(POSITION_PRICE_CURRENT);
            string comment = getCommentFromHistoryPartialClose(ticket_number, 200); //PositionGetString(POSITION_COMMENT);


            if (ticket_number==0)
            {
              res+="error";
              continue;
            }
            
            //RETRIEVE OPEN ORDER'S TYPE
            ticket_order_type = PositionGetInteger(POSITION_TYPE);
            switch(ticket_order_type)
            {
               //BUYING POSITION
               case POSITION_TYPE_BUY:
                  ticket_order_type_name = "BUY";
                  break;
               
               //SELLING POSITION
               case POSITION_TYPE_SELL:
                  ticket_order_type_name = "SELL";
                  break;
            }
            

            string item="{'symbol':'"+symbol+"', "+
            "'qty':"+DoubleToString(vol)+", "+
            "'type': '"+ticket_order_type_name+"', 'id':"+IntegerToString(ticket_number)+", 'open_price':"+
            DoubleToString(open_price)+", 'profit':"+
            DoubleToString(position_profit)+", 'curr_price': "+DoubleToString(curr_price)+", 'SL':"+DoubleToString(sl)+", 'TP':"+
            DoubleToString(tp)+", 'comment': '"+comment+"'}";

            //Print(item);
            
            

            /*Alert(OrderSymbol() + " " + ticket_order_type_name + " Ticket: " + ticket_number + ", Open Price: " +
            OrderOpenPrice() + ", Close Price: " + OrderClosePrice() + ", P/L: " + ticket_profit + " Pips , Stop Loss: " +
            OrderStopLoss() + ", Take Profit: " + OrderTakeProfit());
            */
            if (pos_counter>0)
            {
            res=res+", ";
            }
            res=res+item;
            
        } else 
        {
          Print("Could not query the position with pos idx: "+IntegerToString(pos_counter));
        }
        
      }
   }
   res=res+"]";
   return res;
}


/*
Verifies that the EA is still running
*/
bool CheckServerStatus() {

   // Is _StopFlag == True, inform the client application
   if (IsStopped()) {
     Print("EA IS STOPPED"); // - sending some stuff to REP without being asked?)
      //sendDataToSocket(repSocket, "{'_response': 'EA_IS_STOPPED'}");
      return(false);
   }
   
   // Default
   return(true);
}


string tick2json(MqlTick& tick, string symbol, string topic ="")
{
  string head="{";
  if (StringLen(topic)>0)
  {
    head="{\"t\":\""+topic +"\", ";
  }
  string my_time = IntegerToString(hd_time.getAbsTimeInMs()); //absolute time in ms since 1970
  return(StringFormat(head+"\"datatype\":\"tick\", \"bid\":%f, \"ask\":%f, \"volume\":%u, \"symbol\":\"%s\", \"server_time\": \"%s\", \"terminal_time\":\"%s\"}", 
    tick.bid, tick.ask, tick.volume, symbol, IntegerToString(tick.time_msc), my_time));
   
}

//+------------------------------------------------------------------+
// Generate string for Bid/Ask by symbol
string GetBidAsk(string symbol) {
   MqlTick last_tick;
   if(SymbolInfoTick(symbol,last_tick))
   {
        return tick2json(last_tick, symbol);
   } else {
      PrintFormat("%s: Error %d", __FUNCTION__, GetLastError());
   }
   return "";
}


// Inform Client
void sendDataToSocket(Socket& pSocket, string message, string topic="") {
  //Print("Sending "+message);
  if (StringLen(topic)>0)
  {
    pSocket.sendMore(topic, true);
  }
  ZmqMsg pushReply(message); //StringFormat("%s", message));
  bool res= pSocket.send(pushReply,true); // NON-BLOCKING
  if(!res){
   Print("Sending socket data returned false");
  }
}



//------------------------------------ publish update

string TransactionDescription(const MqlTradeTransaction &trans)
  {
//--- 
   string desc=EnumToString(trans.type)+"\r\n";
   desc+="Symbol: "+trans.symbol+"\r\n";
   desc+="Deal ticket: "+(string)trans.deal+"\r\n";
   desc+="Deal type: "+EnumToString(trans.deal_type)+"\r\n";
   desc+="Order ticket: "+(string)trans.order+"\r\n";
   desc+="Order type: "+EnumToString(trans.order_type)+"\r\n";
   desc+="Order state: "+EnumToString(trans.order_state)+"\r\n";
   desc+="Order time type: "+EnumToString(trans.time_type)+"\r\n";
   desc+="Order expiration: "+TimeToString(trans.time_expiration)+"\r\n";
   desc+="Price: "+StringFormat("%G",trans.price)+"\r\n";
   desc+="Price trigger: "+StringFormat("%G",trans.price_trigger)+"\r\n";
   desc+="Stop Loss: "+StringFormat("%G",trans.price_sl)+"\r\n";
   desc+="Take Profit: "+StringFormat("%G",trans.price_tp)+"\r\n";
   desc+="Volume: "+StringFormat("%G",trans.volume)+"\r\n";
   desc+="Position: "+(string)trans.position+"\r\n";
   desc+="Position by: "+(string)trans.position_by+"\r\n";
//--- den erhaltenen String zurückgeben
   return desc;
  }


/*
handing all transaction changes.
This helps to post-process the signal trades and to republish them
**/

void OnTradeTransaction(const MqlTradeTransaction &trans,
      const MqlTradeRequest &request,
      const MqlTradeResult &result)
{
      ulong            lastOrderID   =trans.order;
      ENUM_ORDER_TYPE  lastOrderType =trans.order_type;
      ENUM_ORDER_STATE lastOrderState=trans.order_state;
      string trans_symbol=trans.symbol;
      ENUM_TRADE_TRANSACTION_TYPE  trans_type=trans.type;
    //see enums
    //https://www.mql5.com/en/docs/constants/tradingconstants/enum_trade_transaction_type
      //Print("Got some transation: "+TransactionDescription(trans));
      //return; 

      
    switch(trans.type)
    {
        //==============================================================================================
        case TRADE_TRANSACTION_DEAL_ADD: // adding an order to history as a result of execution or cancelation
        {
          //This is called when an order has been placed/filled or closed

          if(true)//trans.order_state==ORDER_STATE_FILLED)
          {
            if (trans.position!=trans.order)
            {
              //Print("Is this a CLOSE order?????");
              string direction="CLOSE";
              bool selection_ok=OrderSelect(trans.position);
              double vol=trans.volume; //OrderGetDouble(ORDER_VOLUME_INITIAL); //ORDER_VOLUME_CURRENT);

              string jsons=StringFormat("{'operation': 'new_order', 'direction': '"+direction+"', 'symbol': '"+
                    trans.symbol+"', 'qty':%.2f, 'order_id': %d, 'price': %f, 'comment': '"+request.comment+"'}", 
                    vol,
                    trans.position,
                    trans.price);
             
              sendDataToSocket(pubSocket, jsons);
              //Print(jsons);
            } else {
              //its a new order
              string direction="";
              if (trans.deal_type==DEAL_TYPE_BUY)
              {
                direction="BUY";
              }
              if (trans.deal_type==DEAL_TYPE_SELL)
              {
                direction="SELL";
              }
              string jsons=StringFormat("{'datatype':'event', 'operation': 'new_order', 'direction': '"+direction+"', 'symbol': '"+
                    trans.symbol+"', 'qty':%.2f, 'order_id': %d, 'price': %f, 'comment': '"+request.comment+"'}", 
                    trans.volume,
                    trans.order,
                    trans.price);
            
              sendDataToSocket(pubSocket, jsons); //(jsons);
              //Print(jsons);

            }
            
          }
        }
      break;
    }

}
