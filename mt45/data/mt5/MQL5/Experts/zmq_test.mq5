/** This is a signal republishing EA.
It listens on all trade activities and republishes new orders and position modifications.

These operations are supported:
{'operation': 'new_order', direction': s, 'symbol': s, 'qty':f, 'order_id':int}

* Action: string, 'SELL', or 'BUY'
* qty: float, the amount of position to open in LOTs
* symbol: string, e.g. 'EURUSD'
* order_id: the id of the order


{'operation': 'update',  'sl': f, 'tp':f, symbol', 'order_id':int}

* sl: stop loss, float
* tp: take profit, float



*/


#property copyright "Copyright 2020, FH"
#property link      "https://www.funkyhedge.com"
#property version   "1.00"

#include <Zmq/Zmq.mqh>
extern string ZEROMQ_PROTOCOL = "tcp";
//extern string HOSTNAME = "*";
extern string HOSTNAME = "localhost";

extern int PUSH_PORT = 32768;
extern int PULL_PORT = 32769;
extern int PUB_PORT = 32770;
extern int MILLISECOND_TIMER = 1;

string Publish_Symbols[1] = {
   "EURUSD" //,"GBPUSD","USDJPY","USDCAD","AUDUSD","NZDUSD","USDCHF"
 };


// CREATE ZeroMQ Context
 Context context();

// CREATE ZMQ_PUB SOCKET
//Socket pubSocket(context, ZMQ_PUB);
Socket pubSocket(context, ZMQ_REQ);





//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
   EventSetTimer(60);
   context.setBlocky(false);
      // Send new market data to PUB_PORT that client is subscribed to.
   Print("[PUB] Binding MT4 Server to Socket on Port " + IntegerToString(PUB_PORT) + "..");
   pubSocket.connect(StringFormat("%s://%s:%d", ZEROMQ_PROTOCOL, HOSTNAME, PUSH_PORT));
   //pubSocket.bind(StringFormat("%s://%s:%d", ZEROMQ_PROTOCOL, HOSTNAME, PUB_PORT));
   //pubSocket.setSendHighWaterMark(1);
   pubSocket.setLinger(0);

   return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
 void OnDeinit(const int reason)
 {

  //pubSocket.unbind(StringFormat("%s://%s:%d", ZEROMQ_PROTOCOL, HOSTNAME, PUB_PORT));
  
//--- destroy timer
  EventKillTimer();

}

/**
Publishes a string to zMQ
*/
void publish(string s)
{
  ZmqMsg request(s);
  pubSocket.send(request, true);
  //according to protocl implementation, we need to collect also the response
  ZmqMsg reply();
  pubSocket.recv(reply, true /*no wait*/);
}


//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
  /*
  Use this OnTick() function to send market data to subscribed client.
   */
 if(!IsStopped())
 {
  for(int s = 0; s < ArraySize(Publish_Symbols); s++)
  {
         // Python clients can subscribe to a price feed by setting
         // socket options to the symbol name. For example:
         // To be used for getting recent/latest price quotes
          MqlTick Latest_Price; // Structure to get the latest prices      
          SymbolInfoTick(Symbol() ,Latest_Price); // Assign current prices to structur

          double ask_1 =Latest_Price.ask;

          string ask_str=DoubleToString(ask_1);
          //Print("Sending " + Publish_Symbols[s] + " " + ask_str + " to PUB Socket");
          string sout=StringFormat("%s %s", Symbol(), ask_str);
          publish(sout);
        }
      }

}

string TransactionDescription(const MqlTradeTransaction &trans)
  {
//--- 
   string desc=EnumToString(trans.type)+"\r\n";
   desc+="Symbol: "+trans.symbol+"\r\n";
   desc+="Deal ticket: "+(string)trans.deal+"\r\n";
   desc+="Deal type: "+EnumToString(trans.deal_type)+"\r\n";
   desc+="Order ticket: "+(string)trans.order+"\r\n";
   desc+="Order type: "+EnumToString(trans.order_type)+"\r\n";
   desc+="Order state: "+EnumToString(trans.order_state)+"\r\n";
   desc+="Order time type: "+EnumToString(trans.time_type)+"\r\n";
   desc+="Order expiration: "+TimeToString(trans.time_expiration)+"\r\n";
   desc+="Price: "+StringFormat("%G",trans.price)+"\r\n";
   desc+="Price trigger: "+StringFormat("%G",trans.price_trigger)+"\r\n";
   desc+="Stop Loss: "+StringFormat("%G",trans.price_sl)+"\r\n";
   desc+="Take Profit: "+StringFormat("%G",trans.price_tp)+"\r\n";
   desc+="Volume: "+StringFormat("%G",trans.volume)+"\r\n";
   desc+="Position: "+(string)trans.position+"\r\n";
   desc+="Position by: "+(string)trans.position_by+"\r\n";
//--- den erhaltenen String zurückgeben
   return desc;
  }
  

/*
handing all transaction changes.
This helps to post-process the signal trades and to republish them
**/

void OnTradeTransaction(const MqlTradeTransaction &trans,
      const MqlTradeRequest &request,
      const MqlTradeResult &result)
{
      ulong            lastOrderID   =trans.order;
      ENUM_ORDER_TYPE  lastOrderType =trans.order_type;
      ENUM_ORDER_STATE lastOrderState=trans.order_state;
      string trans_symbol=trans.symbol;
      ENUM_TRADE_TRANSACTION_TYPE  trans_type=trans.type;
    //see enums
    //https://www.mql5.com/en/docs/constants/tradingconstants/enum_trade_transaction_type
      Print("Got some transation: "+TransactionDescription(trans));
      //return; 

      
    switch(trans.type)
    {
        //==============================================================================================
        case TRADE_TRANSACTION_DEAL_ADD: // adding an order to history as a result of execution or cancelation
        {
          //This is called when an order has been placed/filled or closed

          if(true)//trans.order_state==ORDER_STATE_FILLED)
          {
            if (trans.position!=trans.order)
            {
              Print("Is this a CLOSE order?????");
              string direction="CLOSE";
              bool selection_ok=OrderSelect(trans.position);
              double vol=trans.volume; //OrderGetDouble(ORDER_VOLUME_INITIAL); //ORDER_VOLUME_CURRENT);

              string jsons=StringFormat("{'operation': 'new_order', direction': '"+direction+"', 'symbol': '"+
                    trans.symbol+"', 'qty':%.2f, 'order_id': %d, 'price': %f}", 
                    vol,
                    trans.position,
                    trans.price);
              pubSocket.connect(StringFormat("%s://%s:%d", ZEROMQ_PROTOCOL, HOSTNAME, PUSH_PORT));
              publish(jsons);
              Print(jsons);
            } else {
              //its a new order
              string direction="";
              if (trans.order_type==ORDER_TYPE_BUY)
              {
                direction="BUY";
              }
              if (trans.order_type==ORDER_TYPE_SELL)
              {
                direction="SELL";
              }
              string jsons=StringFormat("{'operation': 'new_order', direction': '"+direction+"', 'symbol': '"+
                    trans.symbol+"', 'qty':%.2f, 'order_id': %d, 'price': %f}", 
                    trans.volume,
                    trans.order,
                    trans.price);
              pubSocket.connect(StringFormat("%s://%s:%d", ZEROMQ_PROTOCOL, HOSTNAME, PUSH_PORT));
              publish(jsons);
              Print(jsons);

            }
            
          }
        }
      break;
    }
}


//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
{
//---

}


//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
{
//---

}
//+------------------------------------------------------------------+
