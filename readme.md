# MT5 for Python

## Setup

Pull the docker image from the repository.

```sh
docker login -u <your_user_name_dockerhub>
docker pull funkyhedge/mt5:latest
```

Now you will need to create the docker network  via
```sh
docker network create --subnet=132.19.0.0/16 mt_default
```




## Building

Building is still some how experimental. You should prefer to use the prepared image from the repository.

For MT4 call:
```sh
cd mt45 && ./build_mt4.sh
```


For MT5 call:
```sh
cd mt45 && ./build_mt5.sh
```


## Running MT5

Start the docker with the command
```sh
./run_mounted_mt5.sh <account_cfg> <ea_name> <docker_ip>
```

* account_cfg:
	Possible account configuration files (provided as demo - may not work anymore):
	- mq_37260607
	- You can setup your own account cfg file. Please use the mq_37260607 as an example. 
	You will have to replace fields Server, Login and Password.

* ea_name:
	Supported EA names:
	* zmqtrader

* docker_ip:
	The docker IP of the container to use

Example:

```sh
./run_mounted_mt5.sh mq_37260607 zmqtrader 132.19.0.2
```



## Connecting via VNC

Important: You will have to use VNC in order to setup your broker account configuration ini file.
You will be able to save it in mt5/tmp or elsewhere and then copy it from your host to the directory `mt45/accounts/mt5` directory.



### On same Machine


If you would like to see the UI, use (Remmina) vnc on `<docker_ip>:5900`

Note, the IP is the IP of the virtual interface within the Docker container.


### ON remote host

If you deploy this to a headless remote server on the internet, you might need to use a ssh port forwarder in order to see the UI from your working desktop machine:

1. get the docker-container IP as described above
2. login to the remote server using ssh via
```sh
ssh -L 5900:<docker_ip>:5900 <your_server_ip> -N
```
* replace <docker_ip> with the assigned value from above.
* The <your_server_ip> is the IP of your server, where you started the container.

Now, every connection to your localhost:5900 is tunneled to <your_server_ip> -> <docker_ip>:5900.

3. Now open Remmina towards `localhost:5900`

Note, that if you run multiple MTs on the remote host, use different IPs when building a tunnel.
```sh
ssh -L 5901:<docker_ip1>:5900 <your_server_ip> -N
ssh -L 5902:<docker_ip2>:5900 <your_server_ip> -N
ssh -L 5903:<docker_ip3>:5900 <your_server_ip> -N
...
```



## Testing

In order to test your MT4 or MT5 pipeline, run the tests of the MetatraderConnector.

Goto the `python` directory and execute these commands once

```sh
python3 -m venv venv
source venv/bin/activate
pip install wheel
pip install -r requirements.txt
```

later, when you come back you just need to call
```
source venv/bin/activate
python test_mt.py <docker_container_ip>
```


