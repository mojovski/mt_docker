#!/bin/sh

exit -1 #dont use it

#call me: ./run_mounted_mt5 40857750 signaller
#in order to start the container with name mt5test and to reach the vnc under 172.18.02:59002

account=$1
eaname=$2

#pass the account config file. e.g am_40693944 to load am_40693944.ini

#am_40693944.ini should be located inside the mt4_console/config directory.

#eaname: set it to `zmqtrader` in order to run the zmqtrader EA


#docker options. can be empty or "-d" to send the docker to background
opt=$3

#-d
docker run  $opt --rm \
    --cap-add=SYS_PTRACE \
    --name mt4$account \
    --expose 5900 \
    --log-driver json-file \
    --log-opt max-size=10m \
    --log-opt max-file=3 \
    -w /home/winer/.wine/drive_c/mt4 \
    -v "$(pwd)/mt45/data/mt4_console":/home/winer/.wine/drive_c/mt4 \
    -v "$(pwd)/mt45/run_mt4.sh":/docker/run_mt4.sh \
    funkyhedge/mt4:latest \
    /docker/run_mt4.sh $account $eaname

    #--network host \

